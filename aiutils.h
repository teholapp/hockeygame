#ifndef AIUTILS_H
#define AIUTILS_H

#include <QObject>
#include <QSize>

//GLOBAL AREA DEFS
#define X_LENGTH 15
#define Y_LENGTH 10
#define WORLD_SIZE X_LENGTH*Y_LENGTH


class AIUtils : public QObject
{
    Q_OBJECT
public:
    explicit AIUtils(QObject *parent = 0);
    ~AIUtils();
    enum VerticalState {
        Right,
        Middle,
        Left
    };

    enum HorizontalState {
        Defence,
        Neutral,
        Attack
    };

    void setGameAreaSize(QSize size);
    AIUtils::HorizontalState puckHorizontalPosition(bool homeTeam, const QPoint& puck);
    AIUtils::VerticalState puckVerticalPosition(bool homeTeam, const QPoint& puck);
    int getRandomNumber(int low, int high);
    QPoint randomPosInRect(QRect rect);
    QPoint translatePosByTeam(bool homeTeam, QPoint pos);
    QRect translateRectByTeam(bool homeTeam, const QRect& rect);
    void generateGrid();
    QPoint gridToAbsolutePos(int x, int y);
    QVector2D absolutePosToGridIndex(const QPoint& pos);
    QList<QRect>* grid();
    static QString readFile(const QString& source);
    static int id(int x, int y) {
        return (X_LENGTH*y + x);
    }

    static int calcTorqueDir(int targetAngle, int currentAngle);

    static double degToRad(double deg);

    static double radToDeg(double rad);


signals:

public slots:

private:
    QSize m_gameAreaSize;
    QList<QRect> m_grid;
};

#endif // AIUTILS_H
