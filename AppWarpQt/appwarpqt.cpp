#include <QDebug>
#include <QList>
#include "appwarpqt.h"
#include "appwarp.h"
#include "listener.h"
#include "itemsmodel.h"

using namespace AppWarp;

class AppWarpQtPrivate :
        public ConnectionRequestListener,
        public LobbyRequestListener,
        public NotificationListener,
        public RoomRequestListener,
        public ZoneRequestListener
{
public:
    AppWarpQtPrivate(AppWarpQt *parent=0);
    virtual ~AppWarpQtPrivate();
    virtual void onConnectDone(int result, int reason);
    virtual void onDisconnectDone(int result);
    virtual void onInitUDPDone(int result) { };
    //Lobby
    virtual void onJoinLobbyDone(AppWarp::lobby levent);
    virtual void onLeaveLobbyDone(AppWarp::lobby levent);
    virtual void onSubscribeLobbyDone(AppWarp::lobby levent);
    virtual void onUnsubscribeLobbyDone(AppWarp::lobby levent);
    virtual void onGetLiveLobbyInfoDone(AppWarp::livelobby levent);

    //Notifications
    virtual void onRoomCreated(AppWarp::room rData) {}
    virtual void onRoomDestroyed(AppWarp::room rData) {}
    virtual void onUserLeftRoom(AppWarp::room rData, std::string user) {}
    virtual void onUserJoinedRoom(AppWarp::room rData, std::string user) {}
    virtual void onUserLeftLobby(AppWarp::lobby ldata, std::string user) {}
    virtual void onUserJoinedLobby(AppWarp::lobby ldata, std::string user) {}
    virtual void onChatReceived(AppWarp::chat chatevent);
    virtual void onPrivateChatReceived(std::string sender, std::string message) {}
    virtual void onUpdatePeersReceived(AppWarp::byte update[], int len, bool isUDP) {}
    virtual void onPrivateUpdateReceived(std::string sender, AppWarp::byte update[], int len, bool isUDP) {}
    virtual void onUserChangeRoomProperty(AppWarp::room rData, std::string user,std::map<std::string, std::string> properties, std::map<std::string, std::string>lockTable){}
    virtual void onUserPaused(std::string user,std::string locId,bool isLobby){}
    virtual void onUserResumed(std::string user,std::string locId,bool isLobby){}
    virtual void onGameStarted(std::string sender, std::string id, std::string nextTurn){}
    virtual void onGameStopped(std::string sender, std::string id){}
    virtual void onMoveCompleted(move event){}
    virtual void onNextTurnRequest(std::string lastTurn){}
    //Room request
    virtual void onSubscribeRoomDone(AppWarp::room revent){}
    virtual void onUnsubscribeRoomDone(AppWarp::room revent){}
    virtual void onJoinRoomDone(AppWarp::room revent);
    virtual void onLeaveRoomDone (AppWarp::room revent){}
    virtual void onGetLiveRoomInfoDone(AppWarp::liveroom revent);
    virtual void onSetCustomRoomDataDone (AppWarp::liveroom revent){}
    virtual void onUpdatePropertyDone(AppWarp::liveroom revent){}
    virtual void onLockPropertiesDone(int result){}
    virtual void onUnlockPropertiesDone(int result){}
    //Zone
    virtual void onCreateRoomDone (AppWarp::room revent){}
    virtual void onDeleteRoomDone (AppWarp::room revent){}
    virtual void onGetAllRoomsDone (AppWarp::liveresult res);
    virtual void onGetOnlineUsersDone (AppWarp::liveresult res){}
    virtual void onGetLiveUserInfoDone (AppWarp::liveuser uevent){}
    virtual void onSetCustomUserInfoDone (AppWarp::liveuser uevent){}
    virtual void onGetMatchedRoomsDone(AppWarp::matchedroom mevent){}

    QVariantMap m_connectedRoom;
    ItemsModel *m_rooms;
    AppWarpQt *q_ptr;
    bool m_connected;
    Client *m_appWarpClient;
    Q_DECLARE_PUBLIC(AppWarpQt);
};

AppWarpQtPrivate::AppWarpQtPrivate(AppWarpQt *parent)
    : q_ptr( parent ), m_connected(false)
{
    qRegisterMetaType<QVariantMapList>("QVariantMapList");
    Client::initialize("8bc80473d9d7fb6628a7cd8f0290af0654e376d7470cc0a580db45e3fd94ea7f",
                       "e34a03c699d13f02ea281d1d69d9b9db1e48e57956cacc24dcd5c2afb3852a95","", q_ptr);
    m_appWarpClient = Client::getInstance();
    m_appWarpClient->setConnectionRequestListener(this);
    m_appWarpClient->setLobbyRequestListener(this);
    m_appWarpClient->setRoomRequestListener(this);
    m_appWarpClient->setNotificationListener(this);
    m_appWarpClient->setZoneRequestListener(this);
    m_rooms = new ItemsModel(QStringList() << "name" << "id" << "owner", q_ptr);
    m_connectedRoom.insert("name", "");
    m_connectedRoom.insert("id", "");
}

AppWarpQtPrivate::~AppWarpQtPrivate()
{

}

void AppWarpQtPrivate::onConnectDone(int result, int reason)
{
    Q_Q(AppWarpQt);
    if(result == AppWarp::result_success) {
        qDebug() << "Connection done!";
        m_connected = true;
    }
    else {
        m_connected = false;
    }
    emit q->connectionStatusChanged();
}

void AppWarpQtPrivate::onDisconnectDone(int result)
{
    Q_Q(AppWarpQt);
    m_connected = false;
    emit q->connectionStatusChanged();
}

void AppWarpQtPrivate::onJoinLobbyDone(AppWarp::lobby levent)
{
    qDebug() << "onJoinLobbyDone. Name: " << QString::fromStdString(levent.name);
}

void AppWarpQtPrivate::onLeaveLobbyDone(AppWarp::lobby levent)
{

}

void AppWarpQtPrivate::onSubscribeLobbyDone(AppWarp::lobby levent)
{

}

void AppWarpQtPrivate::onUnsubscribeLobbyDone(AppWarp::lobby levent)
{

}

void AppWarpQtPrivate::onGetLiveLobbyInfoDone(AppWarp::livelobby levent)
{

}

void AppWarpQtPrivate::onGetAllRoomsDone (AppWarp::liveresult res)
{
    Q_Q(AppWarpQt);
    if(res.result == AppWarp::result_success) {
        qDebug() << "Received rooms";
        for (std::vector<std::string>::iterator it = res.list.begin() ; it != res.list.end(); it++) {
            m_appWarpClient->getLiveRoomInfo(*it);
        }
    }
    else {
        qDebug() << "Failed to get rooms.";
    }
}

void AppWarpQtPrivate::onGetLiveRoomInfoDone(AppWarp::liveroom revent)
{
    Q_Q(AppWarpQt);
    qDebug() << "Got live room info";
    if(revent.result == AppWarp::result_success) {
        QVariantMap newRoom;
        newRoom.insert("id", QString::fromStdString(revent.rm.roomId));
        newRoom.insert("owner", QString::fromStdString(revent.rm.owner));
        newRoom.insert("name", QString::fromStdString(revent.rm.name));
        m_rooms->addItem( newRoom );
        qDebug() << "room name: " << newRoom.value("name");
        emit q->roomsChanged();
    }
}

void AppWarpQtPrivate::onJoinRoomDone(AppWarp::room revent)
{
    Q_Q(AppWarpQt);
    if(revent.result == AppWarp::result_success) {
        qDebug() << "Connected to room: " << QString::fromStdString(revent.name);
        m_connectedRoom.insert("name", QString::fromStdString(revent.name));
        m_connectedRoom.insert("id", QString::fromStdString(revent.roomId));
        emit q->connectedRoomChanged();
    }
    else {
        qDebug() << "failed to connect room";
    }
}

void AppWarpQtPrivate::onChatReceived(AppWarp::chat chatevent)
{
    Q_Q(AppWarpQt);
    emit q->chatReceived(QString::fromStdString(chatevent.chat));
}

AppWarpQt::AppWarpQt(QObject *parent)
    :QObject(parent), d_ptr(new AppWarpQtPrivate(this))
{

}


AppWarpQt::~AppWarpQt()
{

}

bool AppWarpQt::connected() const
{
    Q_D(const AppWarpQt);
    return d->m_connected;
}

void AppWarpQt::connectToServer(const QString& playerName)
{
    Q_D(AppWarpQt);
    d->m_appWarpClient->connectServer(playerName.toStdString());
}

void AppWarpQt::getAllRooms()
{
    Q_D(AppWarpQt);
    d->m_appWarpClient->getAllRooms();
}

QObject *AppWarpQt::rooms()
{
    Q_D( AppWarpQt);
    return dynamic_cast<QObject*>(d->m_rooms);
}

 void AppWarpQt::joinRoom(const QString& roomId)
 {
    Q_D(AppWarpQt);
    d->m_appWarpClient->joinRoom(roomId.toStdString());
 }

 QVariantMap AppWarpQt::connectedRoom() const
 {
    Q_D(const AppWarpQt);
    return d->m_connectedRoom;
 }

 void AppWarpQt::sendChat(const QString& msg)
 {
    Q_D(const AppWarpQt);
    d->m_appWarpClient->sendChat(msg.toStdString());
 }
