#ifndef AREATRIGGERCONTEXT_H
#define AREATRIGGERCONTEXT_H

#include <QObject>
#include "contextengine.h"
#include "aiengine.h"

class AreaTriggerContext : public ContextSource
{
public:
    AreaTriggerContext(AIUtils *utils, ContextEngine *engine, QObject *parent=0);
    virtual void addSource(const QString& aName, const QVariant& vVal);

public slots:
    void puckPosChanged(const QPoint& aPos);
    void playerPosChanged(const QPoint& aPos);

private:
    QString name(const QString name);
    void parseAreas();
    QStringList m_activeAreas;
    QMap<QString, QRect> m_gameAreas;
    AIUtils *m_utils; //Not owned

};

#endif // AREATRIGGERCONTEXT_H
