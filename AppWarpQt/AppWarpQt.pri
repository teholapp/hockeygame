#-------------------------------------------------
#
# Project created by QtCreator 2015-01-07T12:12:46
#
#-------------------------------------------------

QT       += network

INCLUDEPATH += AppWarpQt

SOURCES += AppWarpQt/appwarpqt.cpp \
    AppWarpQt/utilities.cpp \
    AppWarpQt/urlencode.cpp \
    AppWarpQt/udpsocket.cpp \
    AppWarpQt/socket.cpp \
    AppWarpQt/SHA1.cpp \
    AppWarpQt/requests.cpp \
    AppWarpQt/HMAC_SHA1.cpp \
    AppWarpQt/base64.cpp \
    AppWarpQt/appwarp.cpp \
    AppWarpQt/cJSON.c \
    AppWarpQt/appwarp_extended.cpp \
    AppWarpQt/itemsmodel.cpp

HEADERS += AppWarpQt/appwarpqt.h\
        AppWarpQt/appwarpqt_global.h \
   AppWarpQt/utilities.h \
    AppWarpQt/urlencode.h \
    AppWarpQt/udpsocket.h \
    AppWarpQt/socket.h \
    AppWarpQt/SHA1.h \
    AppWarpQt/requests.h \
    AppWarpQt/listener.h \
    AppWarpQt/HMAC_SHA1.h \
    AppWarpQt/defines.h \
    AppWarpQt/cJSON.h \
    AppWarpQt/base64.h \
    AppWarpQt/appwarp.h \
    AppWarpQt/itemsmodel.h
