import QtQuick

Item {
    signal roomSelected(var roomId)
    ListView {
        id: listView
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 40
        anchors.rightMargin: 40
        anchors.topMargin: 5
        anchors.bottomMargin: 5
        model: appWarp.rooms
        orientation: Qt.Horizontal
        spacing: 40
        delegate: OpponentDelegate {
            height: parent.height
            name: model.name
            onSelected: roomSelected(model.id)

        }
    }
}

