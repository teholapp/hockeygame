import QtQuick
import Felgo

EntityBase {
    id: puck
    // the enityId should be set by the level file!
    entityType: "puck"

    property real zoomFactor: 1.0
    property alias image: image
    property bool posReached: false
    property alias body: circleCollider.body

    function applyLinearImpulse(impulseVector) {
         circleCollider.applyLinearImpulse(impulseVector, circleCollider.body.getWorldCenter())

   // function applyLinearImpulse(impulseVector) {
   //     var forwardVectorInBody = circleCollider.body.toWorldVector(impulseVector.x, impulseVector.y);
   //     circleCollider.applyLinearImpulse(forwardVectorInBody, circleCollider.body.getWorldCenter());
    }


    property point destination
    property int moveDuration

  /*  PropertyAnimation on x {
      from: puck.x
      to: destination.x
      duration: moveDuration
    }

    PropertyAnimation on y {
      from: puck.y
      to: destination.y
      duration: moveDuration
    }
*/

    Component.onCompleted: {
        console.debug("puck.onCompleted()")
        console.debug("puck.x:", x)
        var mapped = mapToItem(world, x, y)
        console.debug("puck.x world:", mapped.x)
        //image.stop()
    }

    Image {
        id: image
        width: circleCollider.width * 2
        height: circleCollider.height * 2
        anchors.centerIn: parent
        source:  "../assets/puck.png"
    }

    ShaderEffect {
         anchors.fill: image
         property variant source: image
         property color glowColor: "cyan"
         property real radius: dp(4)
         property int samples: 16
         property real spread: 0.8
         scale: 2.0

         fragmentShader: "
             uniform lowp sampler2D source;
             uniform lowp float qt_Opacity;
             uniform highp vec4 glowColor;
             uniform highp float radius;
             uniform highp float spread;
             varying highp vec2 qt_TexCoord0;
             highp vec2 resolution = vec2(textureSize(source, 0));

             void main() {
                 highp vec4 srcColor = texture2D(source, qt_TexCoord0);
                 highp vec4 sum = vec4(0.0);
                 highp float total = 0.0;

                 for (int x = -samples / 2; x <= samples / 2; x++) {
                     for (int y = -samples / 2; y <= samples / 2; y++) {
                         highp vec2 samplePos = qt_TexCoord0 + vec2(x, y) * radius / resolution;
                         highp vec4 sampleColor = texture2D(source, samplePos);
                         highp float weight = 1.0 - (length(vec2(x, y) * radius) / (samples * spread));
                         weight = max(weight, 0.0);
                         sum += sampleColor * weight;
                         total += weight;
                     }
                 }

                 highp vec4 glow = glowColor * (sum / total - srcColor);
                 gl_FragColor = srcColor + glow * qt_Opacity;
             }
         "
     }

    BoxCollider {
        id: circleCollider

        // the image and the physics will use this size; this is important as it specifies the mass of the body! it is in respect to the world size
        width: 2 * zoomFactor
        height: 2 * zoomFactor

        anchors.centerIn: parent


        density: 0.001
        friction: 0.05
        restitution: 0.5
        body.bullet: true
        body.linearDamping: 1
        body.angularDamping: 2
        categories: Box.Category2

        Component.onCompleted: {
            console.debug("puck.physics.x:", x)
            var mapped = mapToItem(world, x, y)
            console.debug("puck.physics.x world:", mapped.x)
        }


        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
        }
    }
}

