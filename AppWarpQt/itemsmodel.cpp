#include "itemsmodel.h"

ItemsModel::ItemsModel(const QStringList& keys, QObject *parent)
    : QAbstractListModel(parent)
 {
    m_roleNames = keys;
}

void ItemsModel::addItem(const QVariantMap &item)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_items.append(item);
    endInsertRows();
}

QHash<int,QByteArray> ItemsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    for (int index = 0; index < m_roleNames.size(); ++index)
        roles.insert(index + Qt::UserRole + 2, m_roleNames.at(index).toLatin1());
    return roles;
}

int ItemsModel::rowCount(const QModelIndex & parent) const
{
    return m_items.count();
}

QVariant ItemsModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid()) return QVariant();

     QVariantMap item = m_items[index.row()];
     if (role == Qt::UserRole)
         return item;

     return item.value(roleNames().value(role));
}



