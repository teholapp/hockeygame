#include <QTimer>
#include <QDebug>
#include <QRect>
#include <QVector2D>
#include <math.h>
#include "aiengine.h"
#include "aiutils.h"
#include "player.h"
#include "team.h"
#include "line.h"
#include "aiplayer.h"
#include "aiplayerpositions.h"

AIPlayer::AIPlayer(bool _homeTeam, Player *_player, AIEngine* _engine, QObject *parent) :
    ContextAction(parent),
    m_engine(_engine),
    m_player(_player),
    m_homeTeam(_homeTeam),
    m_reactionTime(50),
    m_state(IDLE)
{
    QString actionName = m_homeTeam?"HOME":"VISITOR";
    actionName.append(":");
    actionName.append(m_player->typeStr(m_player->type()));
    setAction(actionName);
    QObject::connect(m_player, SIGNAL(positionChanged()), this, SLOT(positionChanged()));

    m_engine->m_contextEngine->addAction(this);
    m_reactionTime = m_engine->m_utils->getRandomNumber(100, 250);
    m_timer = new QTimer(this);
    m_timer->setInterval(m_reactionTime);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(advancePlayer()));
    m_timer->start();
}

AIPlayer::~AIPlayer()
{

}

void AIPlayer::advancePlayer()
{
    if(!m_player->userControlled()) {
        calcTorque();
        calcForce();
        //Handle passing
        if(m_state == PASSPUCK) {
            if(m_path.length() > 0) {
                QPoint subpos = m_path.at(0);
                int x_axis = subpos.x() - m_player->position().x();
                int y_axis = subpos.y() - m_player->position().y();

                if(abs(m_player->torque()) <= 1) {
                    QVector2D dirVect(x_axis, y_axis);
                    qDebug() << "###########PASS!!" << dirVect.x() << dirVect.y();
                    QPointF param(dirVect.x(), dirVect.y());
                    emit m_engine->passToDirection(QVariant::fromValue(param));
                    m_state = IDLE;
                }
            }
            else if(!m_player->puckOwner()){
                qDebug() << "not owned";
                m_path.clear();
                m_state = IDLE;
            }
        }
        else if(m_state == STEALPUCK) {
            followPuck();
        }
    }
}


void AIPlayer::runAction(const QString& action, const QVariant aParam)
{
    if(action == "MOVE") {
        qDebug() << "MOVE";
        QMap<QString, QVariant> targetPoint = aParam.toMap();
        int xIndex = targetPoint.value("x").toInt();
        int yIndex = targetPoint.value("y").toInt();
        QPoint targetPos = m_engine->m_utils->gridToAbsolutePos(xIndex, yIndex);
        QPoint target = m_engine->m_utils->translatePosByTeam(m_homeTeam, targetPos);
        qDebug() << "TARGET: " << target;
        m_path = m_engine->calcPathTo(m_player->position(), target);
        qDebug() << "PATH: " << m_path;
        qDebug() << m_path;
        if(m_path.length() > 0) {
            QPoint subpos = m_path.at(0);
            //subpos = m_engine->m_utils->translatePosByTeam(m_homeTeam, subpos);
            m_player->setTargetPos(subpos);
            m_player->setOnTarget(false);
        }
    }
    else if(action == "PASS") {
        qDebug() << "PASS" << aParam;
        QString targetPlayer = aParam.toString();
        passToPlayer(targetPlayer);
    }
    else if(action == "ATTACK") {
        m_state = ATTACK;
    }
    else if(action == "DEFEND") {
        m_state = DEFEND;
    }
    else if(action == "STEALPUCK") {
        m_state = STEALPUCK;
    }
}

void AIPlayer::positionChanged()
{
    if(m_path.length() > 0) {
        QPoint subpos = m_path.at(0);
        //subpos = m_engine->m_utils->translatePosByTeam(m_homeTeam, subpos);
        QVector2D pos(subpos.x(), subpos.y());
        QVector2D ppos(m_player->position().x(), m_player->position().y());
        float dist = pos.distanceToPoint(ppos);
        if(abs(dist) < 32 && !m_player->onTarget()) {
            if(m_state != PASSPUCK)
                m_path.removeAt(0);
            if(m_path.length() > 0) {
                QPoint subpos = m_path.at(0);
                //subpos = m_engine->m_utils->translatePosByTeam(m_homeTeam, subpos);
                m_player->setTargetPos(subpos);
            }
            else {
                emit m_player->setOnTarget(true);
            }
        }
    }
}

void AIPlayer::moveToPos(const QVariant& aPos)
{
    return;
    QPoint target = aPos.toPoint();
    qDebug() << "moveToPos: " << target;
    target = m_engine->m_utils->translatePosByTeam(m_homeTeam, target);
    m_player->setTargetPos(target);
}


void AIPlayer::calcTorque()
{
    if(m_path.length() == 0) {
        m_player->setTorque(0);
        return;
    }

    QPoint subpos = m_path.at(0);
    //subpos = m_engine->m_utils->translatePosByTeam(m_homeTeam, subpos);

    QVector2D target(subpos.x(), subpos.y());
    QVector2D player(m_player->position().x(), m_player->position().y());

    int x_axis = target.x() - player.x();
    int y_axis = target.y() - player.y();

    double targetAngle = atan2(y_axis, x_axis );

    int direction = AIUtils::calcTorqueDir(AIUtils::radToDeg(targetAngle), m_player->rotation());
    int pixelsPerMeter = m_engine->pixelsPerMeter();
    int torque = direction * 2000 * pixelsPerMeter * pixelsPerMeter;
    m_player->setTorque(torque);
}

void AIPlayer::calcForce()
{
    if(!m_player->onTarget()) {
        QPoint force(400 * pow(m_engine->pixelsPerMeter(), 2), 0);
        m_player->setForce(force);
    }
    else
        m_player->setForce(QPoint(0.0,0.0));

}

void AIPlayer::passToPlayer(const QString& aPlayer)
{
    m_state = PASSPUCK;
    QStringList playerTarget = aPlayer.split(":");
    Team *team = NULL;
    QString type = playerTarget.at(1);
    if(playerTarget.at(0) == "VISITOR") {
        team = m_engine->m_visitorTeam;
    }
    else {
        team = m_engine->m_homeTeam;
    }

    QList<QObject*> lines = team->m_lines;
    bool found = false;
    QList<QObject*>::iterator i;
    for (i = lines.begin(); i != lines.end(); ++i) {
        Line* line = dynamic_cast<Line*>(*i);
        QList<Player*> players = line->m_players;
        QList<Player*>::iterator j;
        for (j = players.begin(); j != players.end(); ++j) {
            Player* player = *j;
            if(player->typeStr(player->type()) == type) {
                found = true;
                m_path.clear();
                m_path.append(QPoint(player->getX(), player->getY()));
                connect(player, &Player::currentPosChanged, this, &AIPlayer::passTargetPosChanged);
            }
        }
    }

    if(!found) {
        m_state = IDLE;
    }
}

void AIPlayer::followPuck()
{
    m_path.clear();
    m_player->setOnTarget(false);
    m_path.append(m_engine->puckPos());
}

void AIPlayer::passTargetPosChanged()
{
    Player* player = qobject_cast<Player*>(sender());

    if(player) {
        if(m_state != PASSPUCK) {
            disconnect(player, &Player::currentPosChanged, this, &AIPlayer::passTargetPosChanged);
        }
        else {
            m_path.append(QPoint(player->getX(), player->getY()));
            m_path.removeAt(0);
        }
    }
}
