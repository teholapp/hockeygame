import Felgo
import QtQuick

Flickable {
    id: gameArea
    anchors.fill: parent
    contentWidth: 720
    contentHeight: 320
    interactive: false

    Rectangle {
        width: 720
        height: 320
        color: "grey"


        // use a physics world because we need collision detection
        PhysicsWorld {
            id: world
            updatesPerSecondForPhysics: 60
        }

        Item {
            id: gameItems
            anchors.fill: parent

            Ring {
                id: ring
                anchors.fill: parent

            }

            Image {
                anchors.fill: parent
                source: "../assets/ring_image.png"
                visible: true
            }

        }
    }

    Rectangle {
        id: drawRect
        color: "red"
        opacity: 0.5
    }

    MouseArea {

        property real lastX: 0
        property real lastY: 0
        property real slashStartPosX: 0
        property real slashStartPosY: 0

        anchors.fill: parent
        property bool pressed: false

        onClicked: {
            if(mouse.x >= gameArea.contentWidth * 0.5) {
                gameArea.contentX = gameArea.contentWidth * 0.36
            }
            else if(gameArea.contentX > 0 && mouse.x <= gameArea.contentWidth * 0.5) {
                gameArea.contentX = 0
            }

        }

        onPressed: {
            pressed = true
            drawRect.x = mouse.x
            drawRect.y = mouse.y
            lastX = mouse.x
            lastY = mouse.y
        }
        onReleased: {
            pressed = false
            lastX = mouse.x
            console.log("Qt.rect(" + drawRect.x + ", " + drawRect.y + " ," + drawRect.width + ", " + drawRect.height + ")")

        }

        onMouseXChanged: {
            var delta = Math.abs(lastX - mouse.x)
            drawRect.width = delta;


        }
        onMouseYChanged: {
            var delta = Math.abs(lastY - mouse.y)
            drawRect.height = delta;

        }
    }
}

