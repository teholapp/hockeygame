#include "team.h"
#include "player.h"
#include "line.h"

Team::Team(QObject *parent) :
    QObject(parent),
    m_name(""),
    m_activeLine(0),
    m_activeGoalie(0),
    m_teamOwnsPuck(false),
    m_homeTeam(false)
{

}

Team::~Team()
{

}

QString Team::name() const
{
    return m_name;
}

void Team::setName(const QString& name)
{
    m_name = name;
}

QVariant Team::lines() const
{
    return QVariant::fromValue(m_lines);
}

Line* Team::line() const
{
    return qobject_cast<Line*>(m_lines.at(activeLine()));
}

void Team::addLine(Line* _line)
{
    m_lines.append(_line);
}

QVariant Team::goalies() const
{
    return QVariant::fromValue(m_goalies);
}

void Team::addGoalie(Player* _goalie)
{
    m_goalies.append(_goalie);
}

int Team::activeLine() const
{
    return m_activeLine;
}

int Team::activeGoalie() const
{
    return m_activeGoalie;
}

void Team::setActiveLine(int _line)
{
    m_activeLine = _line;
    emit activeLineChanged();
}

void Team::setActiveGoalie(int _goalie)
{
    m_activeGoalie = _goalie;
    emit activeGoalieChanged();
}

bool Team::ownsPuck() const
{
    return m_teamOwnsPuck;
}

void Team::setOwnsPuck(bool _owns)
{
    m_teamOwnsPuck = _owns;
    emit puckOwnerChanged();
}


bool Team::homeTeam() const
{
    return m_homeTeam;
}
void Team::setHomeTeam(const bool val)
{
    m_homeTeam = val;
}
