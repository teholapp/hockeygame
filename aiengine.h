#ifndef AIENGINE_H
#define AIENGINE_H

#include <QObject>
#include <QPoint>
#include <QJSEngine>
#include <QMap>
#include <QString>
#include <QRect>
#include "contextengine.h"
#include "aiplayer.h"

class Team;
class AIUtils;
class ContextEngine;
class AreaTriggerContext;

class AIEngine : public QObject
{
    Q_OBJECT
public:
    explicit AIEngine(ContextEngine *contextEngine, QObject *parent = 0);

    Q_PROPERTY(QPoint puckPos READ puckPos WRITE setPuckPos NOTIFY puckPosChanged)
    Q_PROPERTY(int width READ width WRITE setWidth)
    Q_PROPERTY(int height READ height WRITE setHeight)
    Q_PROPERTY(int pixelsPerMeter READ pixelsPerMeter WRITE setPixelsPerMeter NOTIFY pixelsPerMeterChanged)
    Q_PROPERTY(QObject* homeTeam READ homeTeam WRITE setHomeTeam)
    Q_PROPERTY(QObject* visitorTeam READ visitorTeam WRITE setVisitorTeam)
    ~AIEngine();

    QPoint puckPos() const;
    void setPuckPos(const QPoint& newPos);
    void setHomeTeam(QObject* _team);
    void setVisitorTeam(QObject* _team);
    QObject* homeTeam() const;
    QObject* visitorTeam() const;
    int height() const;
    void setHeight(int _height);
    int width() const;
    void setWidth(int _width);
    Q_INVOKABLE void initialize();
    Q_INVOKABLE void homeTeamGotPuck();
    Q_INVOKABLE void visitorTeamGotPuck();
    Q_INVOKABLE void noOneHasPuck();
    void initTeam(Team *team);

    void parseFormations();
    bool isPlayerOnPos(int x, int y);
    QList<QPoint> calcPathTo(QPoint from, QPoint to);
    int pixelsPerMeter() const;
    void setPixelsPerMeter(const int val);
signals:
    void homeTeamGotPuckSignal();
    void visitorTeamGotPuckSignal();
    void noOneHasPuckSignal();
    void pixelsPerMeterChanged();
    void puckPosChanged(const QPoint& aPos);
    void passToDirection(const QVariant value);
public slots:

protected:
    AIUtils *m_utils;

private:
    QPoint m_puckPos;
    Team *m_homeTeam;
    Team *m_visitorTeam;
    int m_width;
    int m_height;
    QList <AIPlayer*> m_aiPlayers;
    QList<QRect> m_graph;

    ContextEngine *m_contextEngine;
    QJSEngine *m_ruleEngine;
    AreaTriggerContext *m_areaHandler;
    int m_pixelsPerMeter;
    friend class AIPlayer;
};

#endif // AIENGINE_H
