#ifndef PATHFINDING_H
#define PATHFINDING_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QMap>
#include <searchcell.h>
class PathFinding : public QObject
{
    Q_OBJECT
public:
    explicit PathFinding(QObject *parent = 0);
    virtual ~PathFinding();
    void findPath(QVector2D currentPos, QVector2D targetPos);
    QVector2D nextPathPos();
    void clearOpenList() { m_openList.clear(); }
    void clearVisitedList() { m_visitedList.clear(); }
    void clearPathToGoal() { m_pathToGoal.clear(); }
    void setBlockedCells(QMap<int, bool>& blocks) {
        m_blockedCells = blocks;
    }
    bool m_initializedStartGoal;
    bool m_foundGoal;

signals:

public slots:

private:
    void setStartAndGoal(SearchCell *start, SearchCell *end);
    void pathOpened(int x, int y, float newCost, SearchCell* parent);
    SearchCell *getNextCell();
    void continuePath();

    SearchCell *m_startCell;
    SearchCell *m_goalCell;
    QVector<SearchCell*> m_openList;
    QVector<SearchCell*> m_visitedList;
    QMap<int, bool> m_blockedCells;
public:
    QVector<QVector2D*> m_pathToGoal;

};

#endif // PATHFINDING_H
