import QtQuick
import QtQuick.Controls

MouseArea {
    id: root
    property alias icon: icon.source
    property alias label: label.text
    property int implicitHeight: 100
    property int implicitWidth: 100
    property string componentName: ""
    property string param: ''
    signal ready(var object)
    signal selected(var name)
    property bool checked: false
    property bool checkable: false

    width: 100
    height: 100

    Rectangle {
        border.color:"#6a6363"
        anchors.fill: parent
        gradient: checked?onn:off

         Gradient {
            id:off
            GradientStop { position: 0.0; color: "lightsteelblue" }
            GradientStop { position: 1.0; color: "black" }
        }

        Gradient {
            id:onn
            GradientStop { position: 0.0; color: "steelblue" }
            GradientStop { position: 1.0; color: "black" }
        }
    }

    Column {
        anchors.fill: parent
        spacing: 5
        Image {
            id: icon
            height: parent.height * 0.75
            width: parent.height * 0.75
            anchors.horizontalCenter: parent.horizontalCenter
            smooth: true
            fillMode: Image.PreserveAspectFit
        }
        Text {
            id: label
            height: 14
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            color: checked?"lightgray":"white"
            font.pixelSize: 14
        }
    }
    onClicked : {
        if(checkable)
           checked = !checked;
    }
}
