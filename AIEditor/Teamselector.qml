import QtQuick
import QtQuick.Controls

Rectangle {
    property alias checked: checkbox.checked
    color: "lightgray"
    border.color: "black"
    border.width: 2
    radius: 5
    width: 200
    height: 80

    Text {
        anchors.margins: 20
        text: "Select the team which owns the puck"
    }

    Row {
        anchors.centerIn: parent
        spacing: 10
        Text { text: "Home"}
        Switch { id: checkbox; checked: false }
        Text { text: "Visitor" }
    }
}

