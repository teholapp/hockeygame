/****************************************************************************
** Meta object code from reading C++ file 'appwarpqt.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../appwarpqt.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'appwarpqt.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_AppWarpQt_t {
    QByteArrayData data[16];
    char stringdata[180];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AppWarpQt_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AppWarpQt_t qt_meta_stringdata_AppWarpQt = {
    {
QT_MOC_LITERAL(0, 0, 9), // "AppWarpQt"
QT_MOC_LITERAL(1, 10, 23), // "connectionStatusChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 12), // "roomsChanged"
QT_MOC_LITERAL(4, 48, 20), // "connectedRoomChanged"
QT_MOC_LITERAL(5, 69, 12), // "chatReceived"
QT_MOC_LITERAL(6, 82, 3), // "msg"
QT_MOC_LITERAL(7, 86, 15), // "connectToServer"
QT_MOC_LITERAL(8, 102, 10), // "playerName"
QT_MOC_LITERAL(9, 113, 11), // "getAllRooms"
QT_MOC_LITERAL(10, 125, 8), // "joinRoom"
QT_MOC_LITERAL(11, 134, 6), // "roomId"
QT_MOC_LITERAL(12, 141, 8), // "sendChat"
QT_MOC_LITERAL(13, 150, 9), // "connected"
QT_MOC_LITERAL(14, 160, 5), // "rooms"
QT_MOC_LITERAL(15, 166, 13) // "connectedRoom"

    },
    "AppWarpQt\0connectionStatusChanged\0\0"
    "roomsChanged\0connectedRoomChanged\0"
    "chatReceived\0msg\0connectToServer\0"
    "playerName\0getAllRooms\0joinRoom\0roomId\0"
    "sendChat\0connected\0rooms\0connectedRoom"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AppWarpQt[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       3,   70, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    0,   56,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       7,    1,   60,    2, 0x02 /* Public */,
       9,    0,   63,    2, 0x02 /* Public */,
      10,    1,   64,    2, 0x02 /* Public */,
      12,    1,   67,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void, QMetaType::QString,    6,

 // properties: name, type, flags
      13, QMetaType::Bool, 0x00495001,
      14, QMetaType::QObjectStar, 0x00495001,
      15, QMetaType::QVariantMap, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void AppWarpQt::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AppWarpQt *_t = static_cast<AppWarpQt *>(_o);
        switch (_id) {
        case 0: _t->connectionStatusChanged(); break;
        case 1: _t->roomsChanged(); break;
        case 2: _t->connectedRoomChanged(); break;
        case 3: _t->chatReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->connectToServer((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->getAllRooms(); break;
        case 6: _t->joinRoom((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->sendChat((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (AppWarpQt::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AppWarpQt::connectionStatusChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (AppWarpQt::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AppWarpQt::roomsChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (AppWarpQt::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AppWarpQt::connectedRoomChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (AppWarpQt::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AppWarpQt::chatReceived)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject AppWarpQt::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AppWarpQt.data,
      qt_meta_data_AppWarpQt,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *AppWarpQt::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AppWarpQt::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_AppWarpQt.stringdata))
        return static_cast<void*>(const_cast< AppWarpQt*>(this));
    return QObject::qt_metacast(_clname);
}

int AppWarpQt::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = connected(); break;
        case 1: *reinterpret_cast< QObject**>(_v) = rooms(); break;
        case 2: *reinterpret_cast< QVariantMap*>(_v) = connectedRoom(); break;
        default: break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 3;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void AppWarpQt::connectionStatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void AppWarpQt::roomsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void AppWarpQt::connectedRoomChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void AppWarpQt::chatReceived(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
