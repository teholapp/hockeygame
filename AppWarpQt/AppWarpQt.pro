#-------------------------------------------------
#
# Project created by QtCreator 2015-01-07T12:12:46
#
#-------------------------------------------------

QT       += network
QT       -= gui

TARGET = AppWarpQt
TEMPLATE = lib

DEFINES += APPWARPQT_LIBRARY

SOURCES += appwarpqt.cpp \
    utilities.cpp \
    urlencode.cpp \
    udpsocket.cpp \
    socket.cpp \
    SHA1.cpp \
    requests.cpp \
    HMAC_SHA1.cpp \
    base64.cpp \
    appwarp.cpp \
    cJSON.c \
    appwarp_extended.cpp \
    itemsmodel.cpp

HEADERS += appwarpqt.h\
        appwarpqt_global.h \
    utilities.h \
    urlencode.h \
    udpsocket.h \
    socket.h \
    SHA1.h \
    requests.h \
    listener.h \
    HMAC_SHA1.h \
    defines.h \
    cJSON.h \
    base64.h \
    appwarp.h \
    itemsmodel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
