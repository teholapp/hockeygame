import QtQuick

Item
{
    property var targetComponent: undefined
    onTargetComponentChanged: {
        if(targetComponent) {
            leftTopCorner.parent = targetComponent
            rightBottomCorner.parent = targetComponent
            topRightCorner.parent = targetComponent
            bottomLeftCorner.parent = targetComponent
        }
    }

    MouseArea {
        id: leftTopCorner
        anchors.left: targetComponent.left
        anchors.top: targetComponent.top
        anchors.leftMargin: -40
        anchors.topMargin: -40
        property int origX: 0
        property int origY: 0
        onPressed: {
            origX = mouse.x
            origY = mouse.y
        }
        onMouseXChanged: {
            var deltaX = origX - mouse.x
            targetComponent.width = targetComponent.width + deltaX
            targetComponent.x = targetComponent.x - deltaX
        }

        onMouseYChanged: {
            var deltaY = origY - mouse.y
            targetComponent.y = targetComponent.y - deltaY
            targetComponent.height = targetComponent.height + deltaY
        }

        width: 80
        height: 80
        drag.target: leftTopCorner
    }

    MouseArea {
        id: rightBottomCorner
        anchors.right: targetComponent.right
        anchors.bottom: targetComponent.bottom
        anchors.rightMargin: -40
        anchors.bottomMargin: -40
        property int origX: 0
        property int origY: 0
        onPressed: {
            origX = mouse.x
            origY = mouse.y
        }
        onMouseXChanged: {
            var deltaX = origX - mouse.x
            targetComponent.width = targetComponent.width - deltaX
        }

        onMouseYChanged: {
            var deltaY = origY - mouse.y
            targetComponent.height = targetComponent.height - deltaY
        }

        width: 80
        height: 80
        drag.target: rightBottomCorner
    }

    MouseArea {
        id: topRightCorner
        anchors.right: targetComponent.right
        anchors.top: targetComponent.top
        anchors.rightMargin: -40
        anchors.topMargin: -40
        property int origX: 0
        property int origY: 0
        onPressed: {
            origX = mouse.x
            origY = mouse.y
        }
        onMouseXChanged: {
            var deltaX = origX - mouse.x
            targetComponent.width = targetComponent.width - deltaX
        }

        onMouseYChanged: {
            var deltaY = origY - mouse.y
            targetComponent.y = targetComponent.y - deltaY
            targetComponent.height = targetComponent.height + deltaY
        }

        width: 80
        height: 80
        drag.target: topRightCorner
    }

    MouseArea {
        id: bottomLeftCorner
        anchors.left: targetComponent.left
        anchors.bottom: targetComponent.bottom
        anchors.leftMargin: -40
        anchors.bottomMargin: -40
        property int origX: 0
        property int origY: 0
        onPressed: {
            origX = mouse.x
            origY = mouse.y
        }
        onMouseXChanged: {
            var deltaX = origX - mouse.x
            targetComponent.width = targetComponent.width + deltaX
            targetComponent.x = targetComponent.x - deltaX
        }

        onMouseYChanged: {
            var deltaY = origY - mouse.y
            targetComponent.height = targetComponent.height - deltaY
        }

        width: 80
        height: 80
        drag.target: bottomLeftCorner
    }
}
