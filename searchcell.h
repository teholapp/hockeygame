#ifndef SEARCHCELL_H
#define SEARCHCELL_H

#include <QObject>
#include <math.h>
#include "aiutils.h"

class SearchCell : public QObject
{
public:
    SearchCell(QObject *parent=0) :
        QObject(parent),
        m_parentNode(NULL),
        m_xcoord(0),
        m_ycoord(0),
        m_id(0),
        m_G(0),
        m_H(0)
    {

    }

    SearchCell(int x, int y, SearchCell* parentNode, QObject *parent=0) :
        QObject(parent),
        m_parentNode(parentNode),
        m_xcoord(x),
        m_ycoord(y),
        m_id(y * X_LENGTH + x),
        m_G(0),
        m_H(0)
    {

    }

    virtual ~SearchCell()
    {
    }

    int id() const  { return m_id; }
    void setId(int val) { m_id = val; }
    void setParentNode(SearchCell* parent) {
        m_parentNode = parent;
    }
    SearchCell* parentNode() const { return m_parentNode; }
    float x() const { return m_xcoord; }
    float y() const { return m_ycoord; }
    void setX(int val) { m_xcoord = val; }
    void setY(int val) { m_ycoord = val; }
    float h() const { return m_H; }
    float g() const { return m_G; }
    void setH(float val) { m_H = val; }
    void setG(float val) { m_G = val; }
    float f() const { return m_G + m_H; }
    float manhattanDistance(const SearchCell *nodeEnd) const {
        float x = (float)(fabs(this->m_xcoord - nodeEnd->m_xcoord));
        float y = (float)(fabs(this->m_ycoord - nodeEnd->m_ycoord));
        return x + y;
    }

private:
    SearchCell *m_parentNode;
    int m_xcoord;
    int m_ycoord;
    int m_id;
    float m_G;
    float m_H;
};

#endif // SEARCHCELL_H
