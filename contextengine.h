#ifndef CONTEXTENGINE_H
#define CONTEXTENGINE_H

#include <QObject>
#include <QMap>
#include <QList>
#include <QVariant>
#include <QString>

class ContextEngine;

class ContextSource : public QObject
{
    Q_OBJECT
public:
    explicit ContextSource(ContextEngine *aEngine, QObject *parent = 0);

    virtual ~ContextSource();

    QString name();

    void setName(const QString& aName);

    virtual void addSource(const QString& aName, const QVariant& vVal) = 0;

protected:
    ContextEngine* m_engine;
    QString m_name;
};


class ContextAction: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString action READ action WRITE setAction NOTIFY actionChanged)
public:
    explicit ContextAction(QObject *parent = 0);
    ~ContextAction();
    virtual void runAction(const QString& action, const QVariant aParam) = 0;
    void setAction(const QString& aAction);
    QString action() const;
signals:
    void actionChanged();
private:
    QString m_action;
};



class ContextEngine : public QObject
{
    Q_OBJECT
public:
    explicit ContextEngine(QObject *parent = 0);
    ~ContextEngine();
    Q_INVOKABLE void runAction(const QString& target, const QString& action, const QVariant& aValue);
    Q_INVOKABLE QVariant contextValue(const QString& aName);
    Q_INVOKABLE void changeContext(const QString& source, const QVariant& value);
    Q_INVOKABLE void setContextVal(const QString& source, const QVariant& value);
    Q_INVOKABLE void addSource(const QString& source, const QString& name, const QVariant& value);
    void addAction(ContextAction*action);
    void addSource(ContextSource*source);
signals:
    void contextChanged(const QString& source, const QVariant& aState);

private:
    QMap<QString, QVariant> m_contexts;
    QList<ContextAction*> m_actions;
    QList<ContextSource*> m_contextSources;
friend class ContextAction;
};


class ContextRule : public QObject
{
    Q_OBJECT
public:
    explicit ContextRule(ContextEngine *aEngine, QObject *parent = 0);
    ~ContextRule();
    virtual void runRule(const QString& source, const QVariant& value);
public slots:
    void contextChanged(const QString& source, const QVariant& value);
protected:
    ContextEngine* m_engine;
};

#endif // CONTEXTENGINE_H
