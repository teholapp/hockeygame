#ifndef PLAYERGENERATOR_H
#define PLAYERGENERATOR_H

#include <QQuickImageProvider>
#include <QObject>
#include <QColor>
#include <QImage>
#include <QPixmap>

class PlayerGenerator : public QQuickImageProvider
{
public:
    PlayerGenerator();
    ~PlayerGenerator();
    void createSkater(int spriteIndex);
    void createSlash(int spriteIndex);
    void createSettings();
    void drawPart(QPainter* painter, const QString& partSource, const QColor& color, bool gradient = true);
    //QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
    QImage requestImage(const QString &id, QSize * size, const QSize & requestedSize);
    QString generateFilename(const QString& filename, int spriteIndex);

    QColor m_skateColor;
    QColor m_glowesColor;
    QColor m_helmetColor;
    QColor m_pantsColor;
    QColor m_sockColor;
    QColor m_upperBodyColor;
    QImage *m_player;
    QImage m_tmpImg;
    int m_width;
    int m_height;

};

#endif // PLAYERGENERATOR_H
