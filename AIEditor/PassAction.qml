import QtQuick

Item {
    id: root
    property alias text: text.text
    signal ready(var object)
    signal selected(string name)
    height: 100
    width: 300

    Row {
        Rectangle {
            id: from
            width: 100
            height: 100
            border.width: 1
            border.color: "black"
            color: checked?"gray":"white"
            property bool checked: false
            MouseArea  {
                anchors.fill: parent
                onClicked: {
                    from.checked = !from.checked
                }
            }
        }

        Rectangle {
            id: comp
            width: 100
            height: 40
            border.color: "black"
            border.width: 2
            color: "red"
            radius: width / 2

            Text {
                id: text
                font.pointSize: 12
                anchors.centerIn: parent
                enabled: true
                text: "LW"
            }
        }

        Rectangle {
            width: 100
            height: 100
            border.width: 1
            border.color: "black"
        }

    }

}
