import QtQuick
import Felgo

EntityBase {
    id: entity
    entityType: "leftGoal"
    signal goal

    BoxCollider {
        id: ylarima
        bodyType: Body.Static
        //collisionTestingOnlyMode: true
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 2
        width: 2
        collidesWith: Box.Category1

        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
            if(collidedEntity.entityType === "puck") {
                console.log("goal")
                goal()
            }
        }
    }

    BoxCollider {
        id: maaliviiva
        bodyType: Body.Static
        collisionTestingOnlyMode: true
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 2
        width: 2

        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
            if(collidedEntity.entityType === "puck") {
                console.log("goal")
                goal()
            }
        }
    }

    BoxCollider {
        id: leftEdge
        bodyType: Body.Static
        height: 2
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.left: parent.left

    }

    BoxCollider {
        id: rightEdge
        bodyType: Body.Static
        height: 2
        width: parent.width
        anchors.top: parent.top
        anchors.left: parent.left

    }

    BoxCollider {
        id: backEdge
        bodyType: Body.Static
        anchors.top: parent.top
        anchors.left: parent.left
        width: 2
        height: parent.height

    }

    Rectangle {
        anchors.fill: parent
        color: "red"
        // this could be set to true for debugging
        visible: true
    }
}

