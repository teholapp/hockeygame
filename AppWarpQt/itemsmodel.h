#ifndef ROOMSMODEL_H
#define ROOMSMODEL_H

#include <QAbstractListModel>
#include <QVariantMap>
#include <QList>
#include <QHash>
#include <QByteArray>

typedef QList<QVariantMap> QVariantMapList;
Q_DECLARE_METATYPE(QVariantMapList)


class ItemsModel : public QAbstractListModel
 {
    Q_OBJECT
public:
    ItemsModel(const QStringList& roles, QObject *parent = 0);

    void addItem(const QVariantMap &item);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QHash<int,QByteArray> roleNames() const;


private:
    QList<QVariantMap> m_items;
    QStringList m_roleNames;
};


#endif // ROOMSMODEL_H
