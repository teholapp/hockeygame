#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QFileSystemModel>

#include "fileservice.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    FileIO fileio;

    QQmlApplicationEngine engine;
    QFileSystemModel *model = new QFileSystemModel;
    model->setRootPath("/");
    engine.rootContext()->setContextProperty("my_model", model);
    engine.rootContext()->setContextProperty("fileio", &fileio);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
