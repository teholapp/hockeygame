/*
 Copyright (c) 2013 Shephertz Technologies Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 The Software shall be used for Good, not Evil.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#ifndef __APPWARP_SOCKET__
#define __APPWARP_SOCKET__
#include <QObject>
#include <QAbstractSocket>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

class QTcpSocket;

namespace AppWarp
{
    class Client;

    namespace Utility
	{
        class Socket : public QObject
		{
            Q_OBJECT
            Client* _callBack;
            QTcpSocket* _socket;
            
		public:
            Socket(Client* owner, QObject *parent=0);
			~Socket();
			int sockConnect(std::string, short );
			int sockDisconnect();
			int sockSend(char *,int);
			void checkMessages();

        public slots:
            void readyRead();
            void connected();
            void disconnected();
            void error ( QAbstractSocket::SocketError socketError );
            
		};

	}
}

#endif
