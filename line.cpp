#include <QDebug>
#include "line.h"
#include "player.h"

Line::Line(QObject *parent) : QObject(parent)
{
}

Line::~Line()
{

}

QQmlListProperty<Player> Line::players()
{
    return QQmlListProperty<Player>(this, &m_players);
}

QList<Player*> Line::getPlayers()
{
    return m_players;
}

void Line::addPlayer(Player* _player)
{
    m_players.append(_player);
}
