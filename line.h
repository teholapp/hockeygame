#ifndef LINE_H
#define LINE_H

#include <QObject>
#include <QList>
#include <QVariant>
#include <QQmlListProperty>
#include <aiengine.h>
#include <QQmlListProperty>


class Player;

class Line : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Player> players READ players CONSTANT)
public:
    explicit Line(QObject *parent = 0);
    ~Line();
    QQmlListProperty<Player> players();
    void addPlayer(Player* _player);
    QList<Player*> getPlayers();

signals:

public slots:

protected:
    QList<Player*> m_players;

    friend class AIEngine;
    friend class AIPlayer;
};


#endif // LINE_H
