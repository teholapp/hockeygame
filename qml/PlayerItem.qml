import QtQuick
import Felgo
import QtQuick.Particles

EntityBase {
    id: player
    entityType: "player"
    property var prevX: 0
    property var prevY: 0
    property bool debug: false
    property int torque: 0
    property var force: Qt.point(0,0)
    property alias image: image
    property var targetPoint
    property bool posReached: false
    property alias collider: boxCollider
    property alias body: boxCollider.body
    property alias stick: stickCollider.body
    property bool onTarget: false
    property int playerIndex: 0
    signal puckCatched
    property bool userControlled: false
    property bool ownsPuck: false
    property real zoomFactor: 1.0
    property bool wallCollisionHandled: false
    signal aiTriggered
    signal targetReached

    onTorqueChanged: {
        if (torque > 0) {
            console.log("start moving")
            movementTimer.start()
        }
    }


    function handleWallHit() {
        return;
        if (!wallCollisionHandled) {
            console.log("Hit to the wall")
            wallCollisionHandled = true;
            var vx = body.linearVelocity.x
            var vy = body.linearVelocity.y
            var speed = Math.sqrt(vx*vx + vy*vy)
            console.log("speed: " + speed)
            if (speed > 1) { // apply only if we have decent impact speed
                var normal = Qt.point(vx / speed, vy / speed)
                // push player back the way they came from
                var impulseStrength = 100
                var impulse = Qt.point(-normal.x * impulseStrength, -normal.y * impulseStrength)
                    body.linearVelocity = Qt.point(
                    body.linearVelocity.x + impulse.x,
                    body.linearVelocity.y + impulse.y
                )

                // add some spin
                //body.angularVelocity += 300
            }
        }
    }

    function applyLinearImpulse(impulse) {
        body.applyLinearImpulse(impulse, boxCollider.body.getWorldCenter());
    }

    function applyAngularImpulse(angularImpulse) {
      body.applyAngularImpulse(angularImpulse);
    }

    Timer {
        id: movementTimer
        interval: 200
        repeat: true
        running: false
        onTriggered: {
            applyLinearImpulse(player.force)
            applyAngularImpulse(player.torque)
        }
    }

    Component.onCompleted: {
        rotation = 0;
        image.stop()
    }

    onRotationChanged: {
        if(rotation > 360)
            rotation = 0;
        else if(rotation < 0) {
            rotation = 360;
        }
    }

    ParticleSystem {
       id: ps
       anchors.fill: parent

       ImageParticle {
           id: logoParticle
           source: "../assets/snowflake.png"
           color: "white"
           anchors.fill: parent
       }

       Emitter {
           anchors.fill: parent
           anchors.topMargin: -image.width / 2
           emitRate: 3
           lifeSpan: 300
           size: 8
           endSize: 1
           sizeVariation: 4
           enabled: image.running
           velocity: AngleDirection{
               // to the left, 0 is right
               angle: -180
               // 45 degree simulates skating.. :)
               angleVariation: 45
               // Pixels per second
               magnitude: image.width*2
               magnitudeVariation: image.width
          }
       }
   }

    AnimatedSprite {
        id: image
        width: 16*zoomFactor
        height: 32*zoomFactor
        anchors.centerIn: parent
        source:  "../assets/athlete_red.png"
        frameCount: 3
        frameSync: false
        frameRate: 4
        frameWidth: 82
        frameHeight: 170
        interpolate: false
        running: boxCollider.force.x > 0 || boxCollider.force.y > 0
   }


   MovementAnimation {
      id: movementAnim
      target: player
      property: "rotation"

      // outputXAxis is +1 if target is to the right, -1 when to the left and 0 when aiming towards it
      velocity: 6000*puckFollower.outputXAxis
      // alternatively, also the acceleration could be set, depends on how you want the followerEntity to behave

      running: player.onTarget && gameStarted

      // this avoids over-rotating, so rotating further than allowed
      maxPropertyValueDifference: puckFollower.absoluteRotationDifference
    }

    MoveToPointHelper {
      id: puckFollower
      // the entity to move towards
      targetObject: puck
    }

    BoxCollider {
        id: boxCollider

        // the image and the physics will use this size; this is important as it specifies the mass of the body! it is in respect to the world size
        width: image.width / 2
        height: image.height / 2

        categories: Box.Category1

        anchors.left: image.left
        anchors.bottom: image.bottom
        property real targetAngle: 0.0

        density: 0.1
        friction: 0.0
        restitution: 0.5
        body.bullet: true
        body.linearDamping: 1
        body.angularDamping: 2
        bodyType: Body.Dynamic
        property bool onObstacle;

        // move forwards and backwards, with a multiplication factor for the desired speed
        //force: player.force
        //torque: player.torque

        Component.onCompleted: {
            console.debug("player.physics.x:", x)
            var mapped = mapToItem(world, x, y);
            console.debug("player.physics.x world:", mapped.x);
        }

        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target;
            console.debug("player collided with entity", collidedEntity.entityType);
            if(collidedEntity.entityType === "puck") {
                puckCatched();
            }
            else if(collidedEntity.entityType === "wall") {
                handleWallHit();
            }
        }

        fixture.onEndContact:  {
            var collidedEntity = other.getBody().target.entityType;

            if(collidedEntity === "wall" ||
                    collidedEntity === "player") {
                onObstacle = false;
                wallCollisionHandled = false;
            }
        }
    }


    BoxCollider {
        id: stickCollider

        // the image and the physics will use this size; this is important as it specifies the mass of the body! it is in respect to the world size
        width: image.width * 0.4
        height: image.height * 0.4

        categories: Box.Category1

        anchors.right: image.right
        anchors.top: image.top

        density: 0.01
        friction: 0.0
        restitution: 0.5
        body.bullet: false
        body.linearDamping: 5
        body.angularDamping: 15

        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
            if(collidedEntity.entityType === "puck" && !ownsPuck) {
                puckCatched()
            } else {
                handleWallHit();
            }
        }

        fixture.onEndContact:  {
            var collidedEntity = other.getBody().target.entityType;

            if(collidedEntity === "wall") {
                wallCollisionHandled = false;
            }
        }

    }

}

