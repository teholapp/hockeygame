#include "player.h"
#include <QDebug>

Player::Player(int _id,
               const QString &_name,
               const QString &_number,
               Type _type,
               int _speed,
               int _weight,
               int _height,
               int _stamina,
               QObject *parent) :
    QObject(parent),
    m_name(_name),
    m_number(_number),
    m_type(_type),
    m_speed(_speed),
    m_weight(_weight),
    m_stamina(_stamina),
    m_height(_height),
    m_maxSpeed(_speed),
    m_id(_id),
    m_onTarget(true)
{

}

Player::Player(QObject *parent) : QObject(parent)
{
}

Player::~Player()
{

}

int Player::id() const
{
    return m_id;
}

QString Player::number() const
{
    return m_number;
}

QString Player::name() const
{
    return m_name;
}

QPoint Player::position() const
{
    return m_position;
}

void Player::setPosition(const QPoint& pos)
{
    m_position = pos;
    emit positionChanged();
}

QPoint Player::targetPos() const
{
    return m_targetPos;
}

void Player::setTargetPos(const QPoint& pos)
{
    m_targetPos = pos;
    emit targetPosChanged();
}

int Player::getX() const
{
    return m_currentPos.x();
}

void Player::setX(const int x)
{
    m_currentPos.setX(x);
    emit currentPosChanged();
}

int Player::getY() const
{
    return m_currentPos.y();
}

void Player::setY(const int y)
{
    m_currentPos.setY(y);
    emit currentPosChanged();
}

int Player::speed() const
{
    return m_speed;
}

void Player::setSpeed(int _speed)
{
    m_speed = _speed;
}

int Player::weight() const
{
    return m_weight;
}

int Player::height() const
{
    return m_height;
}

int Player::stamina() const
{
    return m_stamina;
}

int Player::type() const
{
    return static_cast<int>(m_type);
}

int Player::rotation() const
{
    return m_rotation;
}
void Player::setRotation(int& _rotation)
{
    m_rotation = _rotation;
    emit rotationChanged();
}

QPoint Player::force() const
{
    return m_force;
}

int Player::torque() const
{
    return m_torque;
}

void Player::setTorque(const int& _torque)
{
    m_torque = _torque;
    emit torqueChanged();
}

void Player::setForce(const QPoint& _force)
{
    m_force = _force;
    emit forceChanged();
}

bool Player::userControlled() const
{
    return m_userControlled;
}

void Player::setUserControlled(const bool controls)
{
    m_userControlled = controls;
    emit userControlledChanged();
}

bool Player::onTarget() const
{
    return m_onTarget;
}

void Player::setOnTarget(const bool value)
{
    m_onTarget = value;
    emit onTargetChanged();
}

QPoint Player::velocity() const
{
    return m_velocity;
}

void Player::setVelocity(const QPoint &_velocity)
{
    m_velocity = _velocity;
    emit velocityChanged();
}

bool Player::puckOwner() const
{
    return m_puckOwner;
}

void Player::setPuckOwner(const bool owns)
{
    m_puckOwner = owns;
    emit puckOwnerChanged();
}

QPointF Player::directionForce() const
{
    return m_directionForce;
}

void Player::setDirectionForce(QPointF newDirectionForce)
{
    if (m_directionForce == newDirectionForce)
        return;
    m_directionForce = newDirectionForce;
    emit directionForceChanged();
}

double Player::rotationForce() const
{
    return m_rotationForce;
}

void Player::setRotationForce(double newRotationForce)
{
    if (m_rotationForce == newRotationForce)
        return;
    m_rotationForce = newRotationForce;
    emit rotationForceChanged();
}
