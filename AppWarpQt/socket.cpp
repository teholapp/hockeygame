/*
 Copyright (c) 2013 Shephertz Technologies Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 The Software shall be used for Good, not Evil.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
#include <fcntl.h>
#include "appwarp.h"
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>

namespace AppWarp
{
	namespace Utility
	{
        
        Socket::Socket(Client* owner, QObject *parent) :
            QObject(parent)
		{
            _socket = new QTcpSocket(this);
            connect(_socket, SIGNAL(connected()), this, SLOT(connected()));
            connect(_socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
            connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
            connect(_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
            _callBack = owner;
		}

		Socket::~Socket()
		{

		}

        void Socket::readyRead()
        {
            //qDebug() << "ready read";
            _callBack->update();
        }

        void Socket::connected()
        {
            qDebug() << "Connected to host";
            _callBack->socketConnectionCallback(AppWarp::result_success);
        }

        void Socket::disconnected()
        {

        }

        void Socket::error ( QAbstractSocket::SocketError socketError )
        {

        }

		int Socket::sockConnect(std::string host, short port)
		{
            QHostAddress hostAddr(QString::fromStdString(host));
            qDebug() << "Connect to host: " << hostAddr.toString();
            _socket->connectToHost(hostAddr, port);
		}

		int Socket::sockDisconnect()
		{
            if(_socket->state() == QAbstractSocket::ConnectedState)
                _socket->disconnectFromHost();
		}

		int Socket::sockSend(char *messageToSend,int messageLength)
		{
            _callBack->keepAliveWatchDog = false;
            int bytes_sent  = _socket->write(messageToSend, messageLength);
            //qDebug() << "bytes sent: " << bytes_sent;
            if(bytes_sent != messageLength)
            {
                return AppWarp::result_failure;
            }
            else
            {
                return AppWarp::result_success;
            }
		}

		void Socket::checkMessages()
		{
            char msg[4096];
            int ret = _socket->read(msg, 4096);
            //qDebug() << "bytes received: " << ret;

            if(ret > 0)
            {
                _callBack->socketNewMsgCallback(msg, ret);
            }
            else
            {
                _callBack->socketConnectionCallback(AppWarp::result_failure);
            }
		}
        
        
	}
}
