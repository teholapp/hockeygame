import QtQuick

Player {
    x: gameArea.contentWidth * 0.4
    y: gameArea.contentHeight * 0.6

    onAiTriggered: calcNewDirections();

    property var playAreaInAttackZone_homeOnLeft:  Qt.rect(469, 217 ,63, 98)
    property var playAreaInAttackZone_homeOnRight:   Qt.rect(194, 11 ,58, 94)
    property var defenceAttackZone_homeOnLeft: Qt.rect(40, 161 ,130, 129)
    property var defenceAttackZone_homeOnRight: Qt.rect(583, 28 ,102, 129)

    function calcNewDirections() {
        var new_x = x
        var new_y = y
        if(team.state == "ownsPuck") {
            if(puckHorizontalPosition() === "attack") {
                var new_pos = randomPosInRect(playAreaInAttackZone_homeOnLeft)
                new_x = new_pos.x
                if(puckVerticalPosition() === "bottom") {
                    new_y = puck.y
                }
                else {
                    new_y = new_pos.y
                }
            }
            else if(puckHorizontalPosition() === "neutral") {
                if(puckVerticalPosition() === "bottom") {
                    new_y = puck.y
                }
                else {
                    new_y = randomPosInRect(playAreaInAttackZone_homeOnLeft).y
                }
                new_x = team.ourSide == "left"?puck.x - 100:puck.x + 100
                if(team.ourSide == "left") {
                    if(new_x < 30)
                        new_x = 30
                }
                else {
                    if(new_x > gameArea.contentWidth - 30)
                        new_x = gameArea.contentWidth - 30
                }

            }
            else if(puckHorizontalPosition() === "defence") {
                if(puckVerticalPosition() === "bottom") {
                    new_y = puck.y
                }
                else {
                    new_y = randomPosInRect(defenceAttackZone_homeOnLeft).y
                }
                new_x = team.ourSide == "left"?puck.x - 100:puck.x + 100
            }
        }
        else {
            new_y = puck.y
        }

        moveToPos(Qt.point(new_x, new_y));
    }

    function goToStartingPos() {
            x = gameArea.contentWidth * 0.4
            y = gameArea.contentHeight * 0.6
    }

    Component.onCompleted: {
        goToStartingPos();
    }

    states: [
        State {
            id: idle
            name: "Idle"
        }
    ]

}

