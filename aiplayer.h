#ifndef AIPLAYER_H
#define AIPLAYER_H

#include <QObject>
#include <QStateMachine>
#include "contextengine.h"

class Team;
class Player;
class QTimer;
class AIEngine;

class AIPlayer : public ContextAction
{
    Q_OBJECT
public:
    explicit AIPlayer(bool _homeTeam, Player *_player, AIEngine* _engine, QObject *parent = 0);
    ~AIPlayer();
    virtual void runAction(const QString& action, const QVariant aParam);
    enum state {
        IDLE,
        DEFEND,
        ATTACK,
        STEALPUCK,
        PASSPUCK,
        SLASH
    };

signals:
    void onTarget();

public slots:
    void advancePlayer();
    void positionChanged();
    void passTargetPosChanged();

private:
    void calcTorque();
    void calcForce();
    void passToPlayer(const QString& aPlayer);
    void moveToPos(const QVariant& aPos);
    void followPuck();

    QTimer *m_timer;
    int m_reactionTime;
    AIEngine *m_engine;
    Player *m_player;
    bool m_homeTeam;
    QList<QPoint> m_path;
    bool m_onTarget;
    state m_state;

};

#endif // AIPLAYER_H
