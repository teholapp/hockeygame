#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>

class QTimer;
class AIEngine;

class GameController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int xAxis READ xAxis WRITE setXAxis NOTIFY xAxisChanged)
    Q_PROPERTY(int yAxis READ yAxis WRITE setYAxis NOTIFY yAxisChanged)
public:
    GameController(AIEngine* engine, QObject *parent);
    ~GameController();
    int xAxis() const;
    int yAxis() const;
    void setXAxis(const int xaxis);
    void setYAxis(const int yaxis);

public slots:
    void calculate();

signals:
    void xAxisChanged();
    void yAxisChanged();

private:
    int m_xaxis;
    int m_yaxis;
    AIEngine *m_engine;
    QTimer *m_advanceTimer;

};

#endif // GAMECONTROLLER_H
