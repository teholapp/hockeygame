import QtQuick
import QtQuick.Controls
import QtQuick.Window
import QtQuick.Dialogs

ApplicationWindow {
    id: root
    title: qsTr("AI Editor")
    width: 920
    height: 460
    visible: true

    property var areas: []
    property var formation : {"RD":{"x":0,"y":0},"LD":{"x":0,"y":0},"LF":{"x":0,"y":0},"C":{"x":0,"y":0},"RF":{"x":0,"y":0}}
    property string selectedArea: ""

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            MenuItem {
                text: qsTr("&Create")
                onTriggered: messageDialog.show(qsTr("Create a new scene?"));
            }
            MenuItem {
                text: qsTr("&Open")
                onTriggered: fileDialog.show(qsTr("Select file to open"));
            }
            MenuItem {
                text: qsTr("&Save")
                onTriggered: {
                    fileio.write("areas.txt", JSON.stringify(areas))
                    var fmtion = {"area":selectedArea, "formation": formation}
                    fileio.write("formation.txt", JSON.stringify(fmtion))
                }
            }
            MenuItem {
                text: qsTr("E&xit")
                onTriggered: Qt.quit();
            }
        }
    }

    Rectangle {
        id: toolbox
        width: 200
        height: 460
        visible: true

        Grid {
            id: tooli
            anchors.fill: parent
            columns: 2

                ToolBarButton {
                    id: frameAreaBtn
                    icon: "areaselect.png"
                    componentName: "AreaSelect.qml"
                    label: "Source"
                    param: '{"width": 100, "height": 100, "x": 10, "y": 10}'
                    onReady: {
                        mainRect.updateAreas(object)
                    }
                    onSelected: {
                        console.log("selected: " + name);
                        selectedArea = name
                    }

                    onPressAndHold: {
                        if(!checked)
                            checked = true;
                        var component = Qt.createComponent(componentName)
                        var object = component.createObject(mainRect, JSON.parse(frameAreaBtn.param));
                        object.selected.connect(frameAreaBtn.selected)
                        object.visible = Qt.binding(function() { return frameAreaBtn.checked })
                        object.ready.connect(frameAreaBtn.ready)
                        drag.target = object
                    }
                    checkable: true
                }
                ToolBarButton {
                    label: qsTr("Pass")
                    icon: "pass.png"
                    checkable: true
                }

                ToolBarButton {
                    label: qsTr("Slash")
                    icon: "slash.png"
                    componentName: "Player.qml"
                    checkable: true
                }
                ToolBarButton {
                    label: qsTr("Steal Puck")
                    icon: "steal.png"
                    componentName: "Player.qml"
                    checkable: true
                }
                ToolBarButton {
                    id: puckOwner
                    label: qsTr("Puck owner")
                    icon: "puck.png"
                    componentName: "Teamselector.qml"
                    checkable: true
                    param: '{"width": 300, "height": 100}'
                    property bool homeTeam: false
                    onHomeTeamChanged: console.log(homeTeam)
                    property var teamSelector: null

                    onClicked: {
                        if(!teamSelector) {
                            console.log("create team selector")
                            var component = Qt.createComponent(componentName)
                            teamSelector = component.createObject(mainRect, JSON.parse(puckOwner.param));
                            puckOwner.homeTeam = Qt.binding(function() {return !teamSelector.checked})
                            teamSelector.visible = Qt.binding(function() { return puckOwner.checked })
                            teamSelector.y = puckOwner.y
                        }
                    }

                }
                ToolBarButton {
                    id: moveButton
                    label: qsTr("Move")
                    icon: "move.png"
                    componentName: "Player.qml"
                    checkable: true
                    param: '{"width": 30, "height": 30}'
                    onSelected: {
                        console.log("player selected")
                    }

                    onPressAndHold: {
                        if(!checked)
                            checked = true;
                        var component = Qt.createComponent(componentName)
                        var object = component.createObject(mainRect, JSON.parse(moveButton.param));
                        object.selected.connect(moveButton.selected)
                        object.visible = Qt.binding(function() { return moveButton.checked })
                        object.ready.connect(moveButton.ready)
                        drag.target = object
                        object.visible = true
                    }
                }
        }
    }

    Rectangle {
        id: mainRect
        height: 460
        width: 720
        x: 200

        function updateFormation(object) {
            var position = {"x":object.x, "y":object.y}

            formation[object.text] = position
            console.log(JSON.stringify(formation))
        }

        function areaSelected(name) {
            selectedArea = name
        }

        function updateAreas(object) {

            var area = {"name": object.text, "x":object.x, "y":object.y, "width":object.width, "height":object.height}
            for (var i = 0; i < areas.length; ++i) {
                console.log(areas[i].name);
                if(areas[i].name === object.text) {
                    console.log("found")
                    areas.splice(i, 1);
                    break;
                }
            }

            areas.push(area)
            console.log(JSON.stringify(areas))

        }

        Component.onCompleted: {
            var str = fileio.read("areas.txt")
            if(str) {
                areas = JSON.parse(str)
                for(var i=0;i<areas.length;i++) {
                    var component = Qt.createComponent("AreaSelect.qml")
                    var area = areas[i]
                    var object = component.createObject(mainRect, {text: area.name, width:area.width, height: area.height, x: area.x, y:area.y});
                    object.ready.connect(mainRect.updateAreas)
                    object.selected.connect(mainRect.areaSelected)
                }
            }
        }

        AIEditor{
            id: ring
            width: 720
            height: 320
        }

        Rectangle {
            id: toolBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: ring.bottom
            SourceSelectionTool {
                id: sourceSelection
            }
            }
        }

   /* AIEditor {
        id: ring
        width: 720
        height: 320
    }*/


    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        currentFolder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
        }
        onRejected: {
            console.log("Canceled")
        }

        function show() {
            fileDialog.title = "Avaa";
            fileDialog.open();
        }

    }



    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }

        onAccepted: {
            root.areas = []
            root.formation = {"RD":{"x":0,"y":0},"LD":{"x":0,"y":0},"LF":{"x":0,"y":0},"C":{"x":0,"y":0},"RF":{"x":0,"y":0}}
        }
    }

}
