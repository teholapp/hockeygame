#include <QTimer>
#include <QTime>
#include <math.h>
#include <QDebug>
#include "line.h"
#include "team.h"
#include "player.h"
#include "aiengine.h"
#include "gamecontroller.h"
#include "aiutils.h"

GameController::GameController(AIEngine *engine, QObject *parent) :
    QObject(parent),
    m_engine(engine)
{
    m_advanceTimer = new QTimer(this);
    connect(m_advanceTimer, &QTimer::timeout, this, &GameController::calculate);
    m_advanceTimer->setInterval(50);
    m_advanceTimer->start();
}

GameController::~GameController()
{

}

void GameController::calculate()
{
    Team* team = qobject_cast<Team*>(m_engine->homeTeam());
    if(team) {
        Player *selectedPlayer = NULL;
        Line* line = team->line();
        foreach(Player* player, line->getPlayers()) {
            if(player && player->userControlled()) {
                selectedPlayer = player;
                break;
            }
        }

        if(selectedPlayer) {
            if(m_xaxis == 0 && m_yaxis == 0) {
                selectedPlayer->setForce(QPoint(0,0));
                selectedPlayer->setTorque(0);
                selectedPlayer->setDirectionForce(QPoint(0,0));
                selectedPlayer->setRotationForce(0);
            }
        else {
            /*    int player_rot = selectedPlayer->rotation();
                double targetAngle = atan2(-m_yaxis, m_xaxis );
                int direction = AIUtils::calcTorqueDir(AIUtils::radToDeg(targetAngle), player_rot);

                int pixelsPerMeter = m_engine->pixelsPerMeter();
                int torque = direction * 1000 * pixelsPerMeter * pixelsPerMeter;
                selectedPlayer->setTorque(torque);

                int f = sqrt(pow(m_xaxis, 2) + pow(m_yaxis, 2));

                QPoint force(f*400 * pow(m_engine->pixelsPerMeter(), 2), 0);
                selectedPlayer->setForce(force);
            */
                // 1) Calculate the direction we want to move in
                //    using xAxis and yAxis
                double angleRadians = std::atan2(-m_yaxis, m_xaxis); // negative y for screen coords
                double speedFactor = std::sqrt(m_xaxis*m_xaxis + m_yaxis*m_yaxis);

                // 2) Apply a linear impulse in that direction
                //    (Tune these numbers to get the "feel" you want)
                int pixelsPerMeter = m_engine->pixelsPerMeter();
                double impulseMagnitude = speedFactor * 300.0 * pixelsPerMeter; // tweak to taste
                double impulseX = impulseMagnitude * std::cos(angleRadians);
                double impulseY = impulseMagnitude * std::sin(angleRadians);

                selectedPlayer->setDirectionForce(QPointF(impulseX, impulseY));

                // 3) Rotation: figure out how to turn
                int playerRot = selectedPlayer->rotation();   // 0..360
                double targetAngleDeg = AIUtils::radToDeg(angleRadians);
                int direction = AIUtils::calcTorqueDir(targetAngleDeg, playerRot);

                // Instead of continuous torque, do a small "angular impulse"
                // to rotate the player’s angular velocity quickly
                double angularImpulse = direction * 400.0 * pixelsPerMeter; // tune up/down
                selectedPlayer->setRotationForce(angularImpulse);

            }
        }
    }
}

int GameController::xAxis() const
{
    return m_xaxis;
}

int GameController::yAxis() const
{
    return m_yaxis;
}



void GameController::setXAxis(const int xaxis)
{
    m_xaxis = xaxis;
    emit xAxisChanged();
}

void GameController::setYAxis(const int yaxis)
{
    m_yaxis = yaxis;
    emit yAxisChanged();
}
