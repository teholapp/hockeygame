#include <QDebug>
#include "pathfinding.h"
#include "aiutils.h"

PathFinding::PathFinding(QObject *parent) :
    QObject(parent),
    m_initializedStartGoal(false),
    m_foundGoal(false)
{

}

PathFinding::~PathFinding()
{

}

void PathFinding::findPath(QVector2D currentPos, QVector2D targetPos)
{
    //qDebug() << currentPos << targetPos;
    if(!m_initializedStartGoal) {
        m_openList.clear();

        m_visitedList.clear();

        m_pathToGoal.clear();

        //Init start
        SearchCell *start = new SearchCell(this);
        start->setX(currentPos.x());
        start->setY(currentPos.y());

        SearchCell *goal = new SearchCell(this);
        goal->setX(targetPos.x());
        goal->setY(targetPos.y());

        setStartAndGoal(start, goal);
        m_initializedStartGoal = true;
    }

    if(m_initializedStartGoal) {
        continuePath();
    }
}

void PathFinding::setStartAndGoal(SearchCell* start, SearchCell* goal)
{
    m_startCell = new SearchCell(start->x(), start->y(), 0, this);
    m_goalCell = new SearchCell(goal->x(), goal->y(), goal, this);

    m_startCell->setG(0);
    m_startCell->setH(m_startCell->manhattanDistance(m_goalCell));
    m_openList.push_back(m_startCell);
}

SearchCell* PathFinding::getNextCell()
{
    float bestF = 999999.0f;
    int cellIndex = -1;
    SearchCell* nextCell = 0;

    for(int i=0;i<m_openList.size(); i++)
    {
        if(m_openList[i]->f() < bestF) {
            bestF = m_openList[i]->f();
            cellIndex = i;
        }
    }

    if(cellIndex >= 0) {
        nextCell = m_openList[cellIndex];
        m_visitedList.push_back(nextCell);
        m_openList.erase(m_openList.begin() + cellIndex);
    }

    return nextCell;
}

void PathFinding::pathOpened(int x, int y, float newCost, SearchCell *parent)
{
    int id = AIUtils::id(x, y);
    if(id < 0 || id > WORLD_SIZE) {
        return;
    }

    if(m_blockedCells[id]) {
        return;
    }

    for(int i = 0; i < m_visitedList.size(); i++) {
        if(id == m_visitedList[i]->id()) {
            return;
        }
    }

    SearchCell* newChild = new SearchCell(x, y, parent, this);
    newChild->setG(newCost);
    newChild->setH(parent->manhattanDistance(m_goalCell));

    for(int j=0;j<m_openList.size(); j++) {
        if(id == m_openList[j]->id()) {
            float newF = newChild->g() + newCost + m_openList[j]->h();
            if(m_openList[j]->f() > newF) {
                m_openList[j]->setG(newChild->g() + newCost);
                m_openList[j]->setParentNode(newChild);
            }
            else { //if the F is not better
                delete newChild;
                return;
            }
        }
    }
    m_openList.push_back(newChild);
}

void PathFinding::continuePath()
{
    if(m_openList.empty())
    {
        return;
    }

    SearchCell* currentCell = getNextCell();
    if(currentCell->id() == m_goalCell->id()) {
        m_goalCell->setParentNode(currentCell->parentNode());
        SearchCell *getPath = NULL;
        for(getPath = m_goalCell; getPath != NULL; getPath = getPath->parentNode())
        {
            m_pathToGoal.push_back(new QVector2D(getPath->x(), getPath->y()));
        }

        m_foundGoal = true;
        return;
    }
    else {
        //rightSide
        pathOpened(currentCell->x() + 1, currentCell->y(), currentCell->g() + 1, currentCell);
        //leftSide
        if(currentCell->x() > 0)
            pathOpened(currentCell->x() - 1, currentCell->y(), currentCell->g() + 1, currentCell);
        //up
        pathOpened(currentCell->x(), currentCell->y()+1, currentCell->g() + 1, currentCell);
        //down
        if(currentCell->y() > 0)
            pathOpened(currentCell->x(), currentCell->y()-1, currentCell->g() + 1, currentCell);
        //left-up diagonal
        if(currentCell->x() > 0)
            pathOpened(currentCell->x()-1, currentCell->y()+1, currentCell->g() + 1.414f, currentCell);
        //right-up diagonal
        pathOpened(currentCell->x()+1, currentCell->y()+1, currentCell->g() + 1.414f, currentCell);
        //left-down diagonal
        if(currentCell->y() > 0 && currentCell->x() > 0)
        pathOpened(currentCell->x()-1, currentCell->y()-1, currentCell->g() + 1.414f, currentCell);
        //right-down diagonal
        if(currentCell->y() > 0)
            pathOpened(currentCell->x()+1, currentCell->y()-1, currentCell->g() + 1.414f, currentCell);



        QVector<SearchCell *>::iterator i;
        for (i = m_openList.begin(); i != m_openList.end(); ++i) {
            if(currentCell->id() == (*i)->id()) {
                m_openList.erase(i);
            }
        }
    }
}
