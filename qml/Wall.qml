import QtQuick
import Felgo

EntityBase {
    id: entity
    entityType: "wall"

    BoxCollider {
        id: boxCollider
        sensor: false
        bodyType: Body.Static
        width: entity.width
        height: entity.height
        restitution: 0.0
        friction: 0.0
        //density: 0
        // the size of the collider is the same as the one from entity by default
    }
}

