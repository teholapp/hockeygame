import QtQuick
import QtQuick.Controls

Item {
    id: root
    property alias source: image.source
    property bool checked: false
    width: 100
    height: 100

    MouseArea {
        anchors.fill: parent
        onClicked : {
            root.checked = !root.checked
        }
    }

    Image {
        id: image
        anchors.fill: parent
        source: "rf.png"
        fillMode: Image.PreserveAspectFit
    }

    // Glow {
    //     anchors.fill: image
    //     radius: 8
    //     samples: 16
    //     color: "blue"
    //     source: image
    //     visible: root.checked
    // }
}

