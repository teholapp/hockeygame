import QtQuick

Item {
    property real zoomFactor: 1.0

    RightTopCornerWall {
        anchors.right: parent.right
        anchors.top: parent.top
        height: 90 * zoomFactor
        width: 90 * zoomFactor
    }

    BottomLeftCornerWall {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        height: 90 * zoomFactor
        width: 90 * zoomFactor
    }

    BottomRightCornerWall {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 90 * zoomFactor
        width: 90 * zoomFactor
    }


    Wall {
        id: border_bottom

        height: 5 * zoomFactor
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Wall {
        id: border_top

        height: 10 * zoomFactor
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
    }

    Wall {
        id: border_left
        width: 10 * zoomFactor
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
    }

    Wall {
        id: border_right
        width: 10 * zoomFactor
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
    }


    LeftTopCornerWall {
        anchors.left: parent.left
        anchors.top: parent.top
        height: 90 * zoomFactor
        width: 90 * zoomFactor
    }
}

