import QtQuick

Player {
    property var posMultiplier: team.ourSide=="left"?1:-1

    function horizontalPos() {
        var new_x = x;
        if(team.state == "ownsPuck") {
            if(puckHorizontalPosition() === "attack")
                new_x =  gameArea.contentWidth * 0.7
            else if(puckHorizontalPosition() === "neutral") {
                new_x = puck.x - 100 * posMultiplier
            }
            else
                new_x = puck.x - 100 * posMultiplier
        }
        else {
            if(puckHorizontalPosition() === "attack")
                new_x =  team.ourSide=="left"?gameArea.contentWidth * 0.7:gameArea.contentWidth * 0.3
            else if(puckHorizontalPosition() === "neutral") {
                new_x = puck.x - 100 * posMultiplier
            }
            else
                new_x = puck.x - 50 * posMultiplier
        }

        if(team.ourSide == "left" && new_x < 20)
            new_x = 20
        else if(team.ourSide == "right" && new_x > gameArea.contentWidth - 20)
            new_x = gameArea.contentWidth - 20
        return new_x
    }
}

