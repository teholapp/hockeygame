import QtQuick
import QtQuick.Controls

Item {


    Row {
        spacing: 10
        SourceSelection {
            id: lf
            source: "rf.png"
        }
        SourceSelection {
            source: "center.png"
        }
        SourceSelection {
            source: "lf.png"
        }
        SourceSelection {
            source: "ld.png"
        }

        SourceSelection {
            source: "rd.png"
        }
        SourceSelection {
            source: "puck.jpg"
        }
    }
}

