#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include <QTextStream>

class FileIO : public QObject
{
    Q_OBJECT

public slots:
    bool write(const QString& source, const QString& data)
    {
        if (source.isEmpty())
            return false;

        QFile file(source);
        if (!file.open(QFile::WriteOnly | QFile::Truncate))
            return false;

        QTextStream out(&file);
        out << data;
        file.close();
        return true;
    }

    QString read(const QString& source)
    {
        if (source.isEmpty())
            return "";

        QFile file(source);
        if (!file.open(QFile::ReadOnly))
            return "";

        QTextStream in(&file);
        QString text;
        text = in.readAll();
        file.close();
        return text;
    }

public:
    FileIO() {}
};

#endif // FILEIO_H
