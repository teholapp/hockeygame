import QtQuick

MouseArea {
    id: root
    signal ready(var object)
    signal selected(var text)
    property alias text: inputArea.text
    property bool sel: false



    Rectangle {
        id: comp
        signal dragFinished
        onDragFinished: inputArea.enabled = true
        width: parent.width
        height: parent.height
        border.color: "#0053b7"
        border.width: 2
        color: sel?"blue":"white"
        opacity: 0.5

        Image {
            anchors.fill: parent
            source: "arrow-corner-all-512.png"
        }

        TextInput {
            id: inputArea
            font.pointSize: 12
            anchors.fill: parent
            anchors.margins: 5
            enabled: true
            onActiveFocusChanged: {
                if(focus) {
                    selected(inputArea.text)
                    sel = true
                }
                else {
                    sel = false
                }
            }

            onAccepted: {
                ready(root)
            }
        }

    }

    ComponentResizer {
        id: resizer
        targetComponent: root
    }

}
