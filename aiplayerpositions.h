#ifndef AIPLAYERPOSITIONS_H
#define AIPLAYERPOSITIONS_H

#include <QRect>

static QRect HT_CENTER_ATTACK(535, 102, 111, 119);
static QRect HT_LW_ATTACK(551, 33, 140, 84);
static QRect HT_RW_ATTACK(558, 200, 138, 87);
static QRect HT_LD_ATTACK(468, 6, 38, 93);
static QRect HT_RD_ATTACK(468, 216, 33, 98);
static QRect HT_CENTER_DEFENCE(73, 117, 101, 91);
static QRect HT_LW_DEFENCE(153, 14, 75, 101);
static QRect HT_RW_DEFENCE(149, 203, 84, 107);
static QRect HT_LD_DEFENCE(56, 85, 81, 54);
static QRect HT_RD_DEFENCE(63, 176, 87, 65);

#endif // AIPLAYERPOSITIONS_H

