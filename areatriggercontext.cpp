#include <QString>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

#include "areatriggercontext.h"
#include "aiutils.h"

const QString EnterValue = "Enter";
const QString LeaveValue = "Leave";
const QString ContextName = "Area";

AreaTriggerContext::AreaTriggerContext(AIUtils *utils, ContextEngine *engine, QObject *parent) :
  ContextSource(engine, parent),
  m_utils(utils)
{
    setName(ContextName);
    parseAreas();
}

void AreaTriggerContext::addSource(const QString& aName, const QVariant& aVal)
{
    QMap<QString, QVariant> targetPoint = aVal.toMap();
    int x1Index = targetPoint.value("x1").toInt();
    int y1Index = targetPoint.value("y1").toInt();
    int x2Index = targetPoint.value("x2").toInt();
    int y2Index = targetPoint.value("y2").toInt();
    QPoint topLeft = m_utils->gridToAbsolutePos(x1Index, y1Index);
    QPoint bottomRight = m_utils->gridToAbsolutePos(x2Index, y2Index);
    QRect area(topLeft, bottomRight);
    m_gameAreas[aName] = area;
}

void AreaTriggerContext::parseAreas()
{
    QString areas = AIUtils::readFile("assets/areas.txt");
    QJsonDocument areasJson = QJsonDocument::fromJson(areas.toUtf8());
    QJsonArray jsonArray = areasJson.array();
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        int x = obj["x"].toInt();
        int y = obj["y"].toInt();
        int width = obj["width"].toInt();
        int height =  obj["height"].toInt();
        QString name = obj["name"].toString();
        QRect rect(x, y, width, height);
        m_gameAreas[name] = rect;
    }
}

void AreaTriggerContext::puckPosChanged(const QPoint& newPos)
{
    QStringList newAreas;
    QMap<QString, QRect>::iterator i;
    for (i = m_gameAreas.begin(); i != m_gameAreas.end(); ++i) {
        QRect area = i.value();
        QRect translatedArea = m_utils->translateRectByTeam(false, area);
        if(area.contains(newPos) || translatedArea.contains(newPos)) {
            newAreas.append(i.key());
            m_engine->setContextVal(name(i.key()), QVariant::fromValue(EnterValue));
        }
    }

    foreach(QString area, m_activeAreas) {
        if(!newAreas.contains(area)) {
            m_engine->setContextVal(name(area), QVariant::fromValue(LeaveValue));
        }
    }
    m_activeAreas = newAreas;
}

void AreaTriggerContext::playerPosChanged(const QPoint& aPos)
{

}

QString AreaTriggerContext::name(const QString name)
{
    return ContextName + ":" + name;
}
