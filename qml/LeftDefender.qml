import QtQuick

Player {
    id: defender
    x: gameArea.contentWidth * 0.4
    y: gameArea.contentHeight * 0.4

    property var playAreaInAttackZone_homeOnLeft:  Qt.rect(468,8,47,92)
    property var playAreaInAttackZone_homeOnRight:  Qt.rect(203,215,49,99)
    property var defenceAttackZone_homeOnLeft: Qt.rect(26.5, 39 ,136, 120)
    property var defenceAttackZone_homeOnRight: Qt.rect(564.7, 156.5 ,130, 119)
    onAiTriggered: calcNewDirections();

    function calcNewDirections() {
        var new_x = x
        var new_y = y
        if(team.state == "ownsPuck") {
            if(puckHorizontalPosition() === "attack") {
                var new_pos = randomPosInRect(playAreaInAttackZone_homeOnLeft)
                new_x = new_pos.x
                if(puckVerticalPosition() === "top") {
                    new_y = puck.y
                }
                else {
                    new_y = new_pos.y
                }
            }
            else if(puckHorizontalPosition() === "neutral") {
                if(puckVerticalPosition() === "top") {
                    new_y = puck.y
                }
                else {
                    new_y = randomPosInRect(playAreaInAttackZone_homeOnLeft).y
                }
                new_x = team.ourSide == "left"?puck.x - 100:puck.x + 100
                if(team.ourSide == "left") {
                    if(new_x < 30)
                        new_x = 30
                }
                else {
                    if(new_x > gameArea.contentWidth - 30)
                        new_x = gameArea.contentWidth - 30
                }

            }
            else if(puckHorizontalPosition() === "defence") {
                if(puckVerticalPosition() === "top") {
                    new_y = puck.y
                }
                else {
                    new_y = randomPosInRect(defenceAttackZone_homeOnLeft).y
                }
                new_x = team.ourSide == "left"?puck.x - 100:puck.x + 100
            }
        }
        else {
            new_y = puck.y
        }

        moveToPos(Qt.point(new_x, new_y));
    }

    function goToStartingPos() {
            x = gameArea.contentWidth * 0.4
            y = gameArea.contentHeight * 0.4
    }

    Component.onCompleted: {
        goToStartingPos();
    }

    states: [
        State {
            id: idle
            name: "idle"
        },
        State {
            id: defence
            name: "defence"
            when: playAreaInAttackZone_homeOnLeft.contains(defender.x, defender.y)
        }/*,
        State {
            id: attack
            name: "attack"
        },
        State {
            id: stealpuck
            name: "steal"
        },
        State {
            id: catchpuck
            name: "catchpuck"
        }*/
    ]

    onStateChanged: print("state changed to: " + state)

}

