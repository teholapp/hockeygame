import QtQuick

Item {
    signal playOnline
    signal practise
    signal settings
    signal editor

    Rectangle {
        anchors.fill: parent
        radius: 8
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#2cb6d0" }
                GradientStop { position: 1.0; color: "#b8ebd8" }
            }

        Image {
            id: title
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 40
            fillMode: Image.PreserveAspectFit
            source: "../assets/logo.png"
        }

        ListModel {
            id: listModel
            ListElement {
                name: qsTr("PLAY ONLINE")
                property bool selectable: false
            }
            ListElement {
                name: qsTr("PRACTISE")
                property bool selectable: true
            }
            ListElement {
                name: qsTr("SETTINGS")
                property bool selectable: true
            }
            ListElement {
                name: qsTr("EDITOR")
                property bool selectable: true
            }
        }

        ListView {
            id: listView
            anchors.top: title.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 40
            model: listModel
            spacing: 40
            delegate: Button {
                width: parent.width
                text: model.name
                property bool selectable: model.selectable
                onClicked: {
                    if(index == 0) {
                        playOnline()
                    }
                    else if(index == 1) {
                        practise()
                    }
                    else if(index == 2) {
                        settings();
                    }
                    else {
                        editor();
                    }
                }
            }
        }
    }
 }

