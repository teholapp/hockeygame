import QtQuick
import Felgo

EntityBase {
    id: cornerArea
    entityType: "wall"

    Repeater {
        id: repeater
        model: 4
        property var prevX: 0
        property var prevY: 0
        property var startAngle: 180
        property var endAngle: 270
        property var step: Math.round(90 / 4)


        PolygonCollider {
           id: collider
           bodyType: Body.Static

           Component.onCompleted: calcCorner()

           function calcXPos(i) {
                return Math.round(Math.cos((Math.PI/180.0)*(repeater.startAngle + (repeater.step * i)))*cornerArea.width + cornerArea.width)
           }

           function calcYPos(i) {
                return Math.round(Math.sin((Math.PI/180.0)*(repeater.startAngle + (repeater.step * i)))*cornerArea.width * -1)
           }

           function calcPrevCorner()
            {
                   return Qt.point(calcXPos(index - 1), calcYPos(index - 1))
            }


           function calcCorner()
            {
               return Qt.point(calcXPos(index), calcYPos(index))
            }

           vertices: [
               calcPrevCorner(),
               calcCorner(),
               Qt.point(calcXPos(index -1), calcYPos(index))
           ]
         }
    }

}
