import QtQuick
import Felgo

EntityBase {
    id: entity
    entityType: "rightGoal"
    signal goal

    BoxCollider {
        id: ylarima
        bodyType: Body.Static
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 2
        width: 2
        collidesWith: Box.Category1

        fixture.onBeginContact: {

            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
            if(collidedEntity.entityType === "puck") {
                goal()
            }
        }
    }

    BoxCollider {
        id: maaliviiva
        bodyType: Body.Static
        collisionTestingOnlyMode: true
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 2
        width: 2

        fixture.onBeginContact: {
            var collidedEntity = other.getBody().target
            console.debug("collided with entity", collidedEntity.entityType)
            if(collidedEntity.entityType === "puck") {
                goal()
            }
        }
    }

    BoxCollider {
        id: leftEdge
        bodyType: Body.Static
        height: 2
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.left: parent.left

    }

    BoxCollider {
        id: rightEdge
        bodyType: Body.Static
        height: 2
        width: parent.width
        anchors.top: parent.top
        anchors.left: parent.left

    }

    BoxCollider {
        id: backEdge
        bodyType: Body.Static
        anchors.top: parent.top
        anchors.right: parent.right
        width: 2
        height: parent.height

    }

    Rectangle {
        anchors.fill: parent
        color: "red"
        // this could be set to true for debugging
        visible: true
    }
}

