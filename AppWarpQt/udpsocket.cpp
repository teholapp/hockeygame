/*
 Copyright (c) 2013 Shephertz Technologies Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 The Software shall be used for Good, not Evil.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "appwarp.h"
#include "requests.h"
#include <fcntl.h>
#include <QDebug>
#include <QUdpSocket>

using namespace std;

namespace AppWarp
{
	namespace Utility
	{
        
        UdpSocket::UdpSocket(Client* owner, QObject *parent)
            :QObject(parent)
		{
            _callBack = owner;
            _socket = new QUdpSocket(this);
            connect(_socket, SIGNAL(connected()), this, SLOT(connected()));
            connect(_socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
            connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
            connect(_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
		}
        
		UdpSocket::~UdpSocket()
		{
            
		}

        void UdpSocket::readyRead()
        {
            qDebug() << "ready read, udp";
            _callBack->update();
        }

        void UdpSocket::connected()
        {
            _callBack->socketConnectionCallback(AppWarp::result_success);
        }

        void UdpSocket::disconnected()
        {

        }

        void UdpSocket::error ( QAbstractSocket::SocketError socketError )
        {

        }

        
        void UdpSocket::socketConnect(string host, short port)
        {
            QHostAddress hostAddr(QString::fromStdString(host));
            _socket->connectToHost(hostAddr, port);
        }
        
        void UdpSocket::sockSend(char *messageToSend,int messageLength)
		{
            _socket->write(messageToSend, messageLength);
		}
        
		void UdpSocket::checkMessages()
		{
            char buf[2048];
            int ret = _socket->read(buf, 2048);
            if(ret > 0){
                if(buf[0] == MessageType::response){
                    response* udpresponse = buildResponse((char*)buf, 0);
                    _callBack->udpresponse(udpresponse);
                    delete udpresponse;
                }
                else if(buf[0] == MessageType::update){
                    notify* notification = buildNotify((char*)buf, 0);
                    _callBack->udpnotify(notification);
                    delete notification;
                }
                _callBack->socketNewMsgCallback(buf, ret);
            }
		}
        
        void UdpSocket::disconnect()
        {
            _socket->disconnect();
        }
        
	}
}
