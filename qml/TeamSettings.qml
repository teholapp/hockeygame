import QtQuick

Item {
    Rectangle {
        anchors.fill: parent
        radius: 8
        color: "#b8ebd8"
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#2cb6d0" }
                GradientStop { position: 1.0; color: "#b8ebd8" }
            }


        Row {
            id: title
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 40
            height: 32
            spacing: 10
            Text {
                id: label
                width: 100
                height: parent.height
                text: qsTr("Your Team")
                color: "white"
                font.pixelSize:  24
                verticalAlignment: Text.AlignVCenter
            }
            Rectangle {
                width: 200
                height: parent.height
                anchors.margins: 20
                anchors.left: label.right
                anchors.right: parent.right
                color: "#2e5c63"
                radius: 8
                TextInput {
                    id: labelInput
                    anchors.fill: parent
                    anchors.leftMargin: 10
                    color: "white"
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 24
                }
            }
        }

        ListModel {
            id: listModel
            ListElement {
                name: qsTr("Helmet")
                selectable: true
            }
            ListElement {
                name: qsTr("Shirt")
                selectable: true
            }
            ListElement {
                name: qsTr("Gloves")
                selectable: true
            }
            ListElement {
                name: qsTr("Pants")
                selectable: true
            }
            ListElement {
                name: qsTr("Socks")
                selectable: true
            }
        }


        ListView {
            id: listView
            anchors.top: title.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: 40
            width: parent.width * 0.3
            model: listModel
            spacing: 10
            delegate: Button {
                id: button
                width: parent.width
                text: model.name
                property bool selectable: model.selectable
                onClicked: {
                    colorSelection.x = button.x + button.width + 80
                    colorSelection.y = listView.y + button.y
                    dimarea.visible = true
                }
            }
        }


        ListModel {
            id: colorModel
            ListElement { color: "black";  }
            ListElement { color: "white";  }
            ListElement { color: "gray";  }
            ListElement { color: "lightBlue";  }
            ListElement { color: "lightRed";  }
            ListElement { color: "red"; }
            ListElement { color: "darkRed"; }
            ListElement { color: "blue";  }
            ListElement { color: "lime";  }
            ListElement { color: "green";  }
            ListElement { color: "darkGreen";  }
            ListElement { color: "gold";  }
            ListElement { color: "yellow"; }
            ListElement { color: "orange";  }

        }

        Image {
            id: playerimage
            source: "image://playerGenerator/settings/black/white/black/gold/white/black"
            anchors.top: title.bottom
            anchors.bottom: listView.bottom
            anchors.left: listView.right
            anchors.right: parent.right
            anchors.rightMargin: 40

            fillMode: Image.PreserveAspectFit
        }

        ShaderEffect {
             anchors.fill: playerimage
             property variant source: playerimage
             property real angle: 45 // Angle in degrees
             property real length: 32 // Length of the blur
             property int samples: 24 // Number of samples along the blur

             fragmentShader: "
                 uniform lowp sampler2D source;
                 uniform lowp float qt_Opacity;
                 varying highp vec2 qt_TexCoord0;
                 highp vec2 resolution = vec2(textureSize(source, 0));

                 highp vec4 directionalBlur() {
                     highp float angleRad = radians(angle);
                     highp vec2 direction = vec2(cos(angleRad), sin(angleRad));
                     highp vec4 color = texture2D(source, qt_TexCoord0);
                     highp float stepSize = length / float(samples) / resolution.x;

                     for (int i = 1; i < samples; ++i) {
                         highp vec2 samplePos = qt_TexCoord0 + direction * stepSize * float(i);
                         color += texture2D(source, samplePos);
                     }

                     return color / float(samples);
                 }

                 void main() {
                     gl_FragColor = directionalBlur() * qt_Opacity;
                 }
             "
         }


        Rectangle {
            id: dimarea
            visible: false
            color: "black"
            opacity: 0.8
            radius: 8
            width: parent.width
            height: parent.height
            Grid {
                id: colorSelection
                columns: 7
                rows: 2
                Repeater {
                    model: colorModel
                    delegate: Rectangle {
                        color: model.color
                        width: 30
                        height: 30
                        border.width: 3
                        border.color: "white"

                        MouseArea  {
                            anchors.fill: parent
                            onClicked: dimarea.visible = false
                        }
                    }
                }
            }
        }
    }
}

