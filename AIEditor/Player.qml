import QtQuick

Item {
    id: root
    property alias text: text.text
    signal ready(var object)
    signal selected(string name)
    visible: true
    Rectangle {
        id: comp
        signal dragFinished
        onDragFinished: inputArea.enabled = true
        width: parent.height
        height: parent.height
        border.color: "black"
        border.width: 2
        color: "red"
        radius: width / 2

        Text {
            id: text
            font.pointSize: 12
            anchors.centerIn: parent
            enabled: true
            text: "LW"
        }
    }

    MouseArea {
        anchors.fill: parent
        drag.target: root
        onReleased: {
            ready(root)
        }
    }


}
