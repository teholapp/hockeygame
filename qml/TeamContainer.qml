import QtQuick
import Game

Item {
    id: team
    signal puckCatched(var player)
    property string playerSprite
    property bool isHomeTeam: true
    property alias theplayer: theplayer
    property real zoomFactor: 1.0

    PlayerItem {
           id: theplayer
           property var myTeam: team.isHomeTeam?homeTeam:visitorTeam
           zoomFactor: team.zoomFactor
           image.source: team.playerSprite
           userControlled: team.isHomeTeam?false:true
           parent: gameItems
           playerIndex: 0
           targetPoint: myTeam.lines[0].players[0].targetPos
           torque: myTeam.lines[0].players[0].rotationForce
           force: myTeam.lines[0].players[0].directionForce
           onTarget: myTeam.lines[0].players[0].onTarget

           Binding {
               target: theplayer.myTeam.lines[0].players[0]
               property: 'rotation'
               value: theplayer.rotation
           }

           Binding {
               target: theplayer.myTeam.lines[0].players[0]
               property: 'userControlled'
               value: theplayer.userControlled
           }

           Binding {
               target: theplayer.myTeam.lines[0].players[0]
               property: 'puckOwner'
               value: theplayer.ownsPuck
           }

           Binding {
               target: theplayer.myTeam.lines[0].players[0]
               property: 'x'
               value: theplayer.x / zoomFactor
           }

           Binding {
               target: theplayer.myTeam.lines[0].players[0]
               property: 'y'
               value: theplayer.y / zoomFactor
           }

           onPuckCatched: {
               myTeam.ownsPuck = true
               team.puckCatched(theplayer)
           }

           Component.onCompleted: {
               x = myTeam.lines[0].players[0].position.x * zoomFactor
               y = myTeam.lines[0].players[0].position.y * zoomFactor
               console.log("team: " + myTeam.homeTeam + " " + myTeam.lines[0].players[0].type)
               //myTeam.lines[0].players[0].type === Player.Center &&
               if( myTeam.homeTeam === true) {
                   gameArea.selectedPlayer = theplayer
                   theplayer.userControlled = true;
               }
               else {
                   theplayer.userControlled = false;
               }

               myTeam.lines[0].players[0].position = Qt.binding(function() { return Qt.point(theplayer.x / zoomFactor, theplayer.y / zoomFactor); })
           }
       }

    /*

    Repeater {
        id: rep
        property var myTeam: team.isHomeTeam?homeTeam:visitorTeam
        model: myTeam.lines[0].players.length

        Player {
               id: theplayer
               image.source: team.playerSprite
               aiControlled: false
               parent: gameItems
               playerIndex: index
               targetPoint: rep.myTeam.lines[0].players[index].position

               onPuckCatched: {
                   rep.myTeam.ownsPuck = true
                   team.puckCatched(theplayer)
               }

               onPosReachedChanged: {
                   if(posReached)
                       rep.myTeam.lines[0].players[index].targetPosReached = true
                   else
                       rep.myTeam.lines[0].players[index].targetPosReached = false
               }

               Component.onCompleted: {
                   x = rep.myTeam.lines[0].players[index].position.x
                   y = rep.myTeam.lines[0].players[index].position.y
                   if(rep.myTeam.lines[0].players[index].type == 0 && team.isHomeTeam == true) {
                       gameArea.selectedPlayer = theplayer
                   }
               }
           }
    }
    */
}


