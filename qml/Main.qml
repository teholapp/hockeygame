import Felgo
import QtQuick
import QtQuick.Controls

GameWindow {
    id: gameWindow

    // you get free licenseKeys as a V-Play customer or in your V-Play Trial
    // with a licenseKey, you get the best development experience:
    //  * No watermark shown
    //  * No license reminder every couple of minutes
    //  * No fading V-Play logo
    // you can generate one from http://v-play.net/license/trial, then enter it below:
    //licenseKey: "<generate one from http://v-play.net/license/trial>"

    activeScene: scene

    // the size of the Window can be changed at runtime by pressing Ctrl (or Cmd on Mac) + the number keys 1-8
    // the content of the logical scene size (480x320 for landscape mode by default) gets scaled to the window size based on the scaleMode
    // you can set this size to any resolution you would like your project to start with, most of the times the one of your main target device
    // this resolution is for iPhone 4 & iPhone 4S
    // Define a property to control the orientation
    property bool isLandscape: true // Change this to false for portrait

    // Adjust the game window size based on orientation
    width: isLandscape ? 1920 : 1080
    height: isLandscape ? 1080 : 1920
    property real zoomFactor: 1.0
    property int blurValue: 12

    property bool gameStarted: false


    Timer {
        interval: 5000
        onTriggered: gameStarted = true
    }

    Timer {
        interval: 5000
        repeat: true
        onTriggered: {
            gameWindow.blurValue += 1
            if (gameWindow.blurValue >= 32)
                gameWindow.blurValue = 32
        }
    }

    EntityManager {
        id: entityManager
        entityContainer: gameItems
    }

    Scene {
        id: scene

        // the "logical size" - the scene content is auto-scaled to match the GameWindow size
        width: isLandscape ? 480 : 320
        height: isLandscape ? 320 : 480
        visible: true

        focus: true
        Keys.forwardTo: [controller1]

        EntityBase {
            id:player1
            TwoAxisController {
                id: controller1
                onInputActionPressed: {
                if(actionName == "fire") {
                   // do something in here, e.g. create a new entity
                   }
                }
            }
        }

        Binding {
            target: gameController
            property: 'xAxis'
            value: controller1.xAxis
        }

        Binding {
            target: gameController
            property: 'yAxis'
            value: controller1.yAxis
        }

        // background rectangle matching the logical scene size (= safe zone available on all devices)
        // see the tutorial "How to create mobile games for different screen sizes and resolutions" in the V-Play doc for more details on content scaling and safe zone

        Flickable {
            id: gameArea
            anchors.fill: parent
            contentWidth: 720 * zoomFactor
            contentHeight: 320 * zoomFactor
            interactive: false
            property var selectedPlayer: ht.theplayer
            property RopeJoint puckJoint: null

            // Use a single property to determine scrolling direction based on orientation
            property string scrollDirection: isLandscape ? "X" : "Y"


            Behavior on contentX {
                NumberAnimation {duration: 250}
            }
            Behavior on contentY {
                NumberAnimation {duration: 250}
            }


            Connections {
                target: aiEngine
                onPassToDirection: {
                    console.log("pass to player: " + value.x + " " + value.y);

                    if(gameArea.puckJoint) {
                        gameArea.puckJoint.destroy();
                    }

                    puck.applyLinearImpulse(
                          Qt.point(50 * value.x,
                                   50 * value.y))

                }
            }

            MouseArea {
                id: mouseArea
                signal slash(int from_x, int from_y, int to_x, int to_y)
                signal pass(int direction, real speed)
                property bool slashing: false
                property real lastX: 0
                property real lastY: 0
                property real slashStartPosX: 0
                property real slashStartPosY: 0

                anchors.fill: parent
                property bool pressed: false


                onPressed: {
                    pressed = true
                    slashing = false
                    slashStartPosX = mouse.x
                    slashStartPosY = mouse.y
                }
                onReleased: {
                    pressed = false
                    puck.x = puck.x
                    puck.y = puck.y
                    lastX = mouse.x

                    if(slashing === true && puck.owner) {
                        slash(slashStartPosX, slashStartPosY, mouse.x, mouse.y)

                    }
                    //puck.owner.ownsPuck = false
                    if(gameArea.puckJoint) {
                        gameArea.puckJoint.destroy();
                    }
                    homeTeam.ownsPuck = false
                    visitorTeam.ownsPuck = false
                    aiEngine.noOneHasPuck();

                }

                onMouseXChanged: {
                    var delta = Math.abs(lastX - mouse.x)
                    if(delta > 60 && slashing == false) {
                        //slashStartPosX = lastX
                        slashing = true
                    }
                    lastX = mouse.x
                    //if(pressed && gameArea.selectedPlayer) gameArea.selectedPlayer.moveToPos(Qt.point(mouse.x, mouse.y));

                }
                onMouseYChanged: {
                    var delta = Math.abs(lastY - mouse.y)
                    if(delta > 60 && slashing == false) {
                        //slashStartPosY = lastY
                        slashing = true
                    }
                    lastY = mouse.y
                    //if(pressed && gameArea.selectedPlayer) gameArea.selectedPlayer.moveToPos(Qt.point(mouse.x, mouse.y));
                }

                onSlash: {
                    if(gameArea.puckJoint) {
                        gameArea.puckJoint.destroy();
                    }

                    console.log("slash: " + (to_x - from_x) + " " + (to_y - from_y));

                    puck.applyLinearImpulse(
                        Qt.point(50 * (to_x - from_x),
                                 50 * (to_y - from_y)))
                }
            }


            Rectangle {
                id: rect
                anchors.fill: parent
                color: "grey"

                Component.onCompleted: {
                    aiEngine.width = rect.width
                    aiEngine.height = rect.height
                }


                // use a physics world because we need collision detection
                PhysicsWorld {
                    id: world
                    updatesPerSecondForPhysics: 120
                    //pixelsPerMeter: aiEngine.pixelsPerMeter
                    Component.onCompleted: {
                        pixelsPerMeter = gameArea.contentWidth / 60
                        aiEngine.pixelsPerMeter = pixelsPerMeter;
                    }
                }

                Item {
                    id: gameItems
                    anchors.fill: parent

                    Image {
                        id: ringImage
                        anchors.fill: parent
                        source: "../assets/ring_image.png"
                        visible: true
                    }

                    Ring {
                        id: ring
                        anchors.fill: parent
                        zoomFactor: gameWindow.zoomFactor
                    }


                    ShaderEffect {
                          anchors.fill: ringImage
                          property variant source: ringImage
                          property real radius: gameWindow.blurValue

                          fragmentShader: "
                              uniform lowp sampler2D source;
                              uniform lowp float qt_Opacity;
                              uniform highp float radius;
                              varying highp vec2 qt_TexCoord0;
                              highp vec2 resolution = vec2(textureSize(source, 0));
                              void main() {
                                  highp vec4 color = vec4(0.0);
                                  highp float total = 0.0;
                                  for (float x = -radius; x <= radius; x++) {
                                      for (float y = -radius; y <= radius; y++) {
                                          highp vec2 samplePos = qt_TexCoord0 + vec2(x, y) / resolution;
                                          highp vec4 sampleColor = texture2D(source, samplePos);
                                          highp float weight = 1.0 - (length(vec2(x, y)) / radius);
                                          weight = weight * weight;
                                          color += sampleColor * weight;
                                          total += weight;
                                      }
                                  }
                                  gl_FragColor = color / total;
                              }
                          "
                      }

                    LeftGoal {
                        height: 39 * zoomFactor
                        width: height * 0.66
                        anchors.verticalCenter: parent.verticalCenter
                        x: 24 * zoomFactor
                        onGoal: console.log("GOOOAAL!!")
                    }


                    RightGoal {
                        height: 39 * zoomFactor
                        width: height * 0.66
                        anchors.verticalCenter: parent.verticalCenter
                        x: gameArea.contentWidth - 50 * zoomFactor
                        onGoal: console.log("GOOOAAL!!")
                    }


                    Puck {
                        id: puck
                        x: gameArea.contentWidth / 2
                        y: gameArea.contentHeight / 2
                        property var owner
                        property point lastPosition: Qt.point(x, y)
                        property int threshold: 100
                        zoomFactor: gameWindow.zoomFactor

                        Component.onCompleted: updateViewport()


                         onXChanged: updateViewport()
                         onYChanged: updateViewport()
                         function updateViewport() {
                             aiEngine.puckPos = Qt.point(x, y);

                             // Center the puck in the viewport unless at the content boundaries
                             var newContentX = x - (gameArea.width / 2);
                             var newContentY = y - (gameArea.height / 2);

                             // Clamp newContentX to ensure it does not exceed the content area or go below zero
                             gameArea.contentX = Math.max(0, Math.min(newContentX, gameArea.contentWidth - gameArea.width));

                             // Clamp newContentY to ensure it does not exceed the content area or go below zero
                             gameArea.contentY = Math.max(0, Math.min(newContentY, gameArea.contentHeight - gameArea.height));

                             // Update last known position of the puck
                             lastPosition = Qt.point(x, y);
                         }
                    }



                    TeamContainer {
                        id: vt
                        isHomeTeam: false
                        playerSprite: "image://playerGenerator/skater/black/yellow/black/white/red/blue"
                        zoomFactor: gameWindow.zoomFactor
                        onPuckCatched: {
                            if(gameArea.puckJoint)
                                gameArea.puckJoint.destroy();
                            gameArea.puckJoint = pkJoint.createObject(world,
                                                    {
                                                      "bodyA": player.body,
                                                      "localAnchorA": Qt.point(5*zoomFactor, 2*zoomFactor),
                                                      "bodyB": puck.body,
                                                      "world": world,
                                                      "collideConnected": false
                                                    })
                        console.log("visitor got the key");
                            aiEngine.visitorTeamGotPuck();
                            puck.owner = player
                            player.ownsPuck = true
                        }
                    }

                    Component {
                        id: pkJoint
                        RopeJoint {
                             maxLength: 5
                           }
                    }

                    TeamContainer {
                        id: ht
                        isHomeTeam: true
                        zoomFactor: gameWindow.zoomFactor
                        playerSprite: "image://playerGenerator/skater/black/blue/cyan/green/yellow/blue"
                        onPuckCatched: {
                            print("puck catched");
                            //puck.x = Qt.binding(function() { return player.x + Math.cos((Math.PI/180.0)*player.rotation + 45)*9 })
                            //puck.y = Qt.binding(function() { return player.y + Math.sin((Math.PI/180.0)*player.rotation + 45)*13 })
                            if(gameArea.puckJoint)
                                gameArea.puckJoint.destroy();
                            gameArea.puckJoint = pkJoint.createObject(world,
                                                                {
                                                                  "bodyA": player.body,
                                                                  "localAnchorA": Qt.point(5*zoomFactor, 10*zoomFactor),
                                                                  "bodyB": puck.body,
                                                                  "world": world,
                                                                  "collideConnected": false
                                                                })
                            aiEngine.homeTeamGotPuck();
                            if(gameArea.selectedPlayer) {
                                gameArea.selectedPlayer.userControlled = true;
                                gameArea.selectedPlayer.ownsPuck = false;
                            }
                            puck.owner = player
                            gameArea.selectedPlayer = player
                            player.userControlled = true;
                            player.ownsPuck = true
                        }
                    }
                }
            }

            DebugDraw {
                anchors.fill: parent
                flags: DebugDraw.AABB | DebugDraw.Joint
                world: world
            }
        }

        Image {
            id: vplayLogo
            source: "../assets/vplay-logo.png"

            // 50px is the "logical size" of the image, based on the scene size 480x320
            // on hd or hd2 displays, it will be shown at 100px (hd) or 200px (hd2)
            // thus this image should be at least 200px big to look crisp on all resolutions
            // for more details, look at the tutorial "How to create mobile games for different screen sizes and resolutions" in the V-Play doc
            width: 50
            height: 50

            // this positions it absolute right and bottom of the GameWindow
            // change resolutions with Ctrl (or Cmd on Mac) + the number keys 1-8 to see the effect
            anchors.right: scene.gameWindowAnchorItem.right
            anchors.bottom: scene.gameWindowAnchorItem.bottom

            // this animation sequence fades the V-Play logo in and out infinitely (by modifying its opacity property)
            SequentialAnimation on opacity {
                loops: Animation.Infinite
                PropertyAnimation {
                    to: 0
                    duration: 2000 // 2 seconds for fade out
                }
                PropertyAnimation {
                    to: 1
                    duration: 2000 // 2 seconds for fade in
                }
            }
        }


        JoystickControllerHUD {
          id: joystickController


          anchors.bottom: parent.bottom
          width: 100; height: 100

          // only show the HUD when in debug build or on desktop
          visible: false //system.debugBuild || system.desktopPlatform

          source: "../assets/joystick_background.png"
          thumbSource: "../assets/joystick_thumb.png"
        }

        Binding {
            target: gameController
            property: 'xAxis'
            value: joystickController.controllerXPosition
        }

        Binding {
            target: gameController
            property: 'yAxis'
            value: joystickController.controllerYPosition
        }
    }

    Rectangle {
        id: dim
        anchors.fill: parent
        opacity: 0.8
        visible: menuStack.visible
        color: "black"
    }


    StackView {
        id: menuStack
        initialItem: mainMenu
        anchors.fill: parent
        anchors.centerIn: parent
        visible: false
        Component {
            id: mainMenu
            MainMenu {
                onPractise: menuStack.visible = false
                onSettings: menuStack.push(teamSettings)
                onEditor: menuStack.push(editorUI)

            }
        }
        Component {
            id: teamSettings
            TeamSettings {

            }
        }

        Component {
            id: editorUI
            AIEditor {

            }

        }
    }


}

