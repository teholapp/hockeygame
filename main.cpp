#include <QApplication>
#include <Felgo/VPApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "team.h"
#include "player.h"
#include "line.h"
#include "aiengine.h"
//#include "appwarpqt.h"
#include "playergenerator.h"
#include "gamecontroller.h"
#include "contextengine.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    VPApplication vplay;

    // QQmlApplicationEngine is the preffered way to start qml projects since Qt 5.2
    // if you have older projects using Qt App wizards from previous QtCreator versions than 3.1, please change them to QQmlApplicationEngine
    QQmlApplicationEngine engine;

    qmlRegisterType<Player>("Game", 1,0, "Player");
    qmlRegisterType<Line>("Game", 1,0, "Line");
    qmlRegisterType<Team>("Game", 1,0, "Team");
    qmlRegisterType<Team>("Game", 1,0, "GameController");

    Player* leftd_ht = new Player(1,
                                  "Teemu",
                                   "66",
                                   Player::LeftDefender,
                                   100,
                                   80,
                                   180,
                                   100);

    Player* rightd_ht = new Player(2,
                                   "Timppa",
                                   "42",
                                   Player::RightDefender,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* center_ht = new Player(3,
                                   "Henkka",
                                   "99",
                                   Player::Center,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* leftw_ht = new Player(4,
                                  "Arto",
                                   "12",
                                   Player::LeftWing,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* rightw_ht = new Player(5,
                                   "Juha",
                                   "33",
                                   Player::RightWing,
                                   100,
                                   80,
                                   180,
                                   100);

    Line *ykkosketju = new Line();
    ykkosketju->addPlayer(leftd_ht);
    ykkosketju->addPlayer(rightd_ht);
    ykkosketju->addPlayer(center_ht);
    ykkosketju->addPlayer(leftw_ht);
    ykkosketju->addPlayer(rightw_ht);

    //Visitor team
    Player* leftd_vt = new Player(6,
                                  "Joe",
                                   "76",
                                   Player::LeftDefender,
                                   100,
                                   80,
                                   180,
                                   100);

    Player* rightd_vt = new Player(7,
                                   "Veikko",
                                   "2",
                                   Player::RightDefender,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* center_vt = new Player(8,
                                   "Reino",
                                   "66",
                                   Player::Center,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* leftw_vt = new Player(9,
                                  "Väinö",
                                   "99",
                                   Player::LeftWing,
                                   100,
                                   80,
                                   180,
                                   100);


    Player* rightw_vt = new Player(10,
                                   "Juha",
                                   "33",
                                   Player::RightWing,
                                   100,
                                   80,
                                   180,
                                   100);

    Line *ykkosketjuvt = new Line();
    ykkosketjuvt->addPlayer(leftd_vt);
    ykkosketjuvt->addPlayer(rightd_vt);
    ykkosketjuvt->addPlayer(center_vt);
    ykkosketjuvt->addPlayer(leftw_vt);
    ykkosketjuvt->addPlayer(rightw_vt);


    Team* hometeam = new Team(&engine);
    hometeam->addLine(ykkosketju);
    hometeam->setHomeTeam(true);
    Team* visitorteam = new Team(&engine);
    visitorteam->addLine(ykkosketjuvt);

    ContextEngine contextEngine;

    AIEngine *aiengine = new AIEngine(&contextEngine, &engine);
    aiengine->setHomeTeam(hometeam);
    aiengine->setVisitorTeam(visitorteam);
    aiengine->initialize();

    //AppWarpQt *appwarp = new AppWarpQt(&engine);

    GameController *controller = new GameController(aiengine, &engine);

    engine.rootContext()->setContextProperty("homeTeam", hometeam);
    engine.rootContext()->setContextProperty("visitorTeam", visitorteam);
    engine.rootContext()->setContextProperty("aiEngine", aiengine);
    //engine.rootContext()->setContextProperty("appWarp", appwarp);
    engine.rootContext()->setContextProperty("gameController", controller);

    engine.addImageProvider(QLatin1String("playerGenerator"), new PlayerGenerator());

    vplay.initialize(&engine);

    // use this during development
    // for PUBLISHING, use the below entry point
    vplay.setMainQmlFileName(QStringLiteral("qml/Main.qml"));

    // use this instead of the above call to avoid deployment of the qml files and compile them into the binary with qt's resource system qrc
    // this is the preferred deployment option for publishing games to the app stores, because then your qml files and js files are protected
    // to avoid deployment of your qml files and images, also comment the DEPLOYMENTFOLDERS command in the .pro file
    // also see the .pro file for more details
    //  vplay.setMainQmlFileName(QStringLiteral("qrc:/qml/Main.qml"));

    engine.load(QUrl(vplay.mainQmlFileName()));

    return app.exec();
}

