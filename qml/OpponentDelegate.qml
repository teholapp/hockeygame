import QtQuick

Item {
    id: root
    property alias name: name.text
    property alias image: image.source
    property alias info: info.text
    width: height
    signal selected
    property bool itemSelected: false

    Column {
        anchors.fill: parent
        Text {
            id: name
            color: "white"

            width: parent.width
            height: 16
            horizontalAlignment: Text.AlignHCenter

        }

        Rectangle {
            id: imageRect
            color: "transparent"
            width: height
            height: parent.height - 32
            anchors.horizontalCenter: parent.horizontalCenter
            ShaderEffect {
                        id: effect
                        anchors.fill: imageFrame
                        property real cornerRadius: imageFrame.radius + 10 // Dynamic corner radius
                        property real glowRadius: 10
                        property real spread: 0.2
                        property color glowColor: "white"
                        visible: root.itemSelected

                        fragmentShader: "
                            uniform lowp sampler2D source;
                            uniform lowp float qt_Opacity;
                            uniform highp float cornerRadius;
                            uniform highp float glowRadius;
                            uniform highp float spread;
                            uniform highp vec4 glowColor;
                            varying highp vec2 qt_TexCoord0;

                            void main() {
                                highp vec2 pos = qt_TexCoord0 * vec2(width, height);
                                highp float dist = distance(pos, vec2(width / 2, height / 2));
                                highp float maxDist = distance(vec2(0.0, 0.0), vec2(width / 2, height / 2));
                                highp float glow = smoothstep(glowRadius, glowRadius * spread, maxDist - dist);

                                gl_FragColor = glowColor * glow * qt_Opacity;
                            }
                        "
                    }
            Rectangle {
                id: imageFrame
                anchors.fill: parent
                border.color: "black"
                radius: 3
                color: "lightgray"
                Image {
                    id: image
                    anchors.fill: parent
                    anchors.margins: 3
                    anchors.centerIn: parent
                    fillMode: Image.PreserveAspectFit
                    source: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQSEhUUExQVFhUVFhQWFBcXFxwUFhcWFxodGBgVFhoZJCggGRooHhYYIzEhJSkrLi4uGB80ODQsNygtLisBCgoKDg0OFBAQGywkHR8tLCwsLCwsLCssLCwrLCwsNywsNywsLCwsLCwsLCwsNywsLDcsKywsKysrKyssKywrLP/AABEIAHAAcAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAFBgMEBwIBAP/EADoQAAECBAQDBgQFAQkAAAAAAAECEQADBCESMUFRBWFxBhMigZGhMlKx8CNCwdHhFBUWM2JygqLi8f/EABoBAAIDAQEAAAAAAAAAAAAAAAIDAAEEBQb/xAAgEQACAgIDAQADAAAAAAAAAAAAAQIRAzEEEiFBEyJR/9oADAMBAAIRAxEAPwD1oSe1vFypfdINkuFEHNWo6CGTtFWmTTrUDfIHYnWM0SSS+1vX/wAPrCYr6Mb+EE0XjxAJV9mL9VSuo6DMdIjk0WIWNzo328MsHqV5kgaH2/kx8uWcx+0XxSkWzOkSV9Op8LG2XXW+sVYXR0U6dK3dDhQYgixBGojS+yfaD+oSULYTU/8AIb9QcxCHIoVh1MQGa+339YJcIqe7my5nMYtHBzfyMDJ2Wo0aciJCI+THQECiHPdaxGpMTgtHCxFkIVRyoRKREaoogldvFNISN1h/IQs0skOzZmGrtxhNOkHMrSE9YVpspcuYEEMbD+R6RcdF0GZfChjsQ1wzO/KDNFwUDRDF7scTaRW4YlzDHRIOkLdmyEVRU/u1iUkhNkt58gA8c1PBhqp2HTyhpkTiAx2tA2rSS+gi3oKK99FPiFCCALhPyjJoB18sDkGdhs0N1em0KtdTqUtKR+YN0vnEiJzJfDSqJeKWg7pSfaLKRFXh6WloGyUj0EWxBGY5UI5Mekx88QhwqI1CJTEaohBO7bUmOnxAsZagryyJ94FVS+9lU8xrsrHyUW9vCYMrrhMlLlzLEpULCx26QryJypUsSV/FiCkHMEDMcs/eLS8DhJaDwkqlsbsWIbPzfKLlJWzMagjEcHxpKchkLiLNAy0sbhhFvBh+29d4CzV1deFyk4kVDV9d3gZxWuWsKw4mSHURc229Y+o5xxzL5h/SOaRZKA2Yf3ilIY06B6QVhIwkBYJBPxdTtvH1LJAmtnhQR0dh9WguqWwKsz0gLLmtMWRoE9N4tesVNdV6OyQwA5RIDCseLzTqB0T+5iE8Rm/OrlpDOpjbG4GOJkwDMgebQoTKlarFRPUkxFnmXi+hVjYutljNabc4gXxSUPz/AKwtFEfYInQllJVz7/fpArtClu6V8qyD/uH/AFg1Jl3f22ij2jMvuihSgFEOBmpxlbPeDoBOg1waewi1WcQJFgOZ/UQC7IVwmJAOYsofQ+0F+KoKGUA6cikWI6Qh7o6WKVxLPCJQWVkkMNX+84hpZ2BahYgX6RHKnSrllJOoBF+txEMuYFLCUAgbkachrA0NVhWqqgQ8AaaYACTmtRCQMywsB7npBPiAEtBJbnAPsfT94tcw3ckDVgM28/pBQf0zZn2pBMU0xeXhHK/vHx4U2b+phwpqNIGUWkUAuWiW2CopCF/TlORfkY9lTMRIyI0hr4lQDaFavlFKgRmIKLYE4IkwR73cSIuHGRZvrHTQ4z2ZvW8WmTNSlPypJA894oCPhHsQAt8Or1SZgWnzG4/eNHo65FTLxJIO4N/Ixl4EFeCioSrFJlrV82FKlJ82y8oCUL0OxZnDZpNNQqbwqts30iBdOEEnbPrzifs9LqpqQVpEoG7PiVydLBn6xzx7s9PmIOGcAcKiRgJdvygg263hTi9M2/nVWhJ7TcY7z8NJsHc7nYQydgJX4WLIXPRySSYz2ZKUksoFJ2IYj1+7Rq/ZSjAkhADjLrteDkklRmxtylbGCRODgOC+Tb7QRnTcIZ4WJPCky5rIODVTknxG4F+Qi9Op1TApOI7ONjZ0884XaHtMtVQcaHpf1hU4rKs+riGGkpu6RhxrX/rzgLxEKUvCgEliW6ffvFqgJ6AvB+KImgoTZSMwcyBbEN4J5Rnk6TNpJoxMFi4u4UDnlppDzw+sTNlhacjm+hGY++UPWjG9meS6BZ/KW3ZgfM2js8MmfKH2xJ/eHynGFaCDiShC0lDsFqUGeZiJxfpzi1w/gPeS5BSs06ZOJKkAB1zAASpyQ+mb5tZnhayNvQThWzMDLLlP5g9tQ2do2HsPMkqoZP4eMIT4yElWFTlx4csuRvrFjhnZ0JlIlomKWZZmKdafiK3YskkFiRYhtc4rdmez5o5XdrNlYcZQt8Mwuy0EDQsAY0Y279QjLXXwYE0wWnFL8LlIAxHJRbGXys5yvFiZRBREsDCU3mPn/kAPym93/KRqYFplzAoJJAnJBUCA6ZiSfEpAf4TmpDuLEaGC0uqE1N/BNlhw/iZ9HtiQSn4vViIe4ozLJK6E7t52dkzUAysYnJ8AsyfiZlvl5AnyvFzsb/hSwLeBNjmCAxB5wcXSGYSpRubqw89H6NaB82WJSnSG32P8xz8slLR1ePFx9ZVmcSwrUFoJOT2Zv1gjRVqFvhBvcukg+uXpFelUiZmzj1/kQQTUSpfhxB+vo8KuKXpt2iWs8QHQQuU6ElcxS9GA21J5aD1glxTi6QnwOo5AAa9YzytoK2eZgDiUrxZ/hnYONTtFKcf6KydlpAXtTxFM+c6C6EpShJ0U2Z9SYm7IVuCbgOS9NlDUc2ceQgTW0MyUppiSknLY9DqI94XMwzpZ0C0k9Hv7RqjVeGGV27NSRNlLsSA2+UdInlITLSykgnARZiSVEYusE/7E3JI+9o5n8GDWJ9Sx8oyqTRvasFUVdMTMKVOQCxQsFRG6RqDs20MC6hnmFjKm3UXYJUdzoku4VoXfOwynxSil3UgE+F2LEMWP3lBCnnsCqV4k2xo/MH5agl7ej5RuwTTOZy4tfCeXNSWlTiWxAy1jwKQoOyhnhLPt5jL2rJlkd6cKvyTkgBCns+yVEAOgllNbK1GbIBQ8rxSwPgF1yxmDL+ZFj4c0tY6R5Q8SVLDH8SUXGWLZwR6PbZxrGtmFMKya9MooQpSQSGAe6iLOHzFvKL65IWLcveAdRJTNSe6U0taRjZTFJSx8JzLptyZMQmtXImFlYkkOkE6uzpe4GVjyjnciKh+x1uPkc2oktLw1KpS1kZOxbJhv5xTpeH4UYyAps3155O9zBeXLCKYl7r06sB9DElSAmlTupvcvHmZ5m21Z1kBJtKkYVJUpDu4bFY/LzjsU6UIOFK1A2IUQbDp+8FOKIaRLGufs/wCsQV9GEzk4bFYu1rjWAUrK9sVu1PDyqnKfiR8Us5lChdnNw7MxjL0WjUuP1ipKZh1ADjIEjX6X5RmE1TklmcksMg8d3gOTg7OdykrP/9k="
                }
            }
        }

        Text {
            id: info
            width: parent.width
            height: 16

            color: "white"
            text: "placeholder"
            horizontalAlignment: Text.AlignHCenter
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: selected()
        onPressed: root.itemSelected = true
        onReleased: root.itemSelected = false
    }
}

