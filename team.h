#ifndef TEAM_H
#define TEAM_H

#include <QObject>
#include <QList>
#include <QVariant>
#include <aiengine.h>

class Line;
class Player;

class Team : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ownsPuck READ ownsPuck WRITE setOwnsPuck NOTIFY puckOwnerChanged)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QVariant lines READ lines CONSTANT)
    Q_PROPERTY(QVariant goalies READ goalies CONSTANT)
    Q_PROPERTY(int activeLine READ activeLine WRITE setActiveLine NOTIFY activeLineChanged)
    Q_PROPERTY(int activeGoalie READ activeGoalie WRITE setActiveGoalie NOTIFY activeGoalieChanged)
    Q_PROPERTY(bool homeTeam READ homeTeam CONSTANT)
public:
    explicit Team(QObject *parent = 0);
    virtual ~Team();
    QString name() const;
    void setName(const QString& name);
    QVariant lines() const;
    void addLine(Line* _line);
    QVariant goalies() const;
    void addGoalie(Player* _goalie);
    int activeLine() const;
    Line *line() const;
    void setActiveLine(int _line);
    int activeGoalie() const;
    void setActiveGoalie(int _goalie);
    bool ownsPuck() const;
    void setOwnsPuck(bool _owns);
    bool homeTeam() const;
    void setHomeTeam(const bool val);

signals:
    void activeLineChanged();
    void activeGoalieChanged();
    void puckOwnerChanged();

public slots:

protected:
    QList<QObject*> m_lines;
    QList<QObject*> m_goalies;
private:
    QString m_name;
    int m_activeLine;
    int m_activeGoalie;
    bool m_teamOwnsPuck;
    bool m_homeTeam;

    friend class AIEngine;
    friend class AIPlayer;
    friend class GameController;
};


#endif // TEAM_H
