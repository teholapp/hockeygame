#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QPoint>

class Player : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString number READ number CONSTANT)
    Q_PROPERTY(QPoint position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(int x READ getX WRITE setX NOTIFY currentPosChanged)
    Q_PROPERTY(int y READ getY WRITE setY NOTIFY currentPosChanged)
    Q_PROPERTY(int speed READ speed WRITE setSpeed NOTIFY speedChanged)
    Q_PROPERTY(int weight READ weight CONSTANT)
    Q_PROPERTY(int stamina READ stamina)
    Q_PROPERTY(int type READ type)
    Q_PROPERTY(int height READ height CONSTANT)
    Q_PROPERTY(QPoint targetPos READ targetPos WRITE setTargetPos NOTIFY targetPosChanged)
    Q_PROPERTY(int rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(int torque READ torque NOTIFY torqueChanged)
    Q_PROPERTY(QPoint force READ force NOTIFY forceChanged)
    Q_PROPERTY(bool userControlled READ userControlled WRITE setUserControlled NOTIFY userControlledChanged)
    Q_PROPERTY(bool onTarget READ onTarget NOTIFY onTargetChanged)
    Q_PROPERTY(QPoint velocity READ velocity WRITE setVelocity NOTIFY velocityChanged)
    Q_PROPERTY(bool puckOwner READ puckOwner WRITE setPuckOwner NOTIFY puckOwnerChanged)
    Q_PROPERTY(QPointF directionForce READ directionForce WRITE setDirectionForce NOTIFY directionForceChanged FINAL)
    Q_PROPERTY(double rotationForce READ rotationForce WRITE setRotationForce NOTIFY rotationForceChanged FINAL)
    Q_ENUMS(Type)

public:
    enum Type { Center, LeftWing, RightWing, LeftDefender, RightDefender, Goalie };

    explicit Player(QObject *parent = 0);

    Player(int _id,
           const QString& _name,
           const QString& _number,
           Type _type,
           int _speed,
           int _weight,
           int _height,
           int _stamina,
           QObject *parent = 0);
    ~Player();

    QString typeStr(int type) {
        switch(type) {
            case Center:
                return "C";
            case LeftWing:
                return "LF";
            case RightWing:
                return "RF";
            case LeftDefender:
                return "LD";
            case RightDefender:
                return "RD";
            case Goalie:
                return "G";
        }
        return "";
    }  

    int id() const;
    QString number() const;
    QString name() const;

    //Set the exact player pos without transition
    QPoint position() const;
    void setPosition(const QPoint& pos);

    //Set the target pos where the player is heading
    QPoint targetPos() const;
    void setTargetPos(const QPoint& pos);

    //Player position on game area
    int getX() const;
    void setX(const int x);

    int getY() const;
    void setY(const int y);

    int speed() const;
    void setSpeed(int _speed);
    int weight() const;
    int height() const;
    int stamina() const;
    int type() const;
    int rotation() const;
    void setRotation(int& _rotation);
    QPoint force() const;
    int torque() const;
    void setTorque(const int& _torque);
    void setForce(const QPoint& _force);
    bool userControlled() const;
    void setUserControlled(const bool controls);
    bool onTarget() const;
    void setOnTarget(const bool value);
    QPoint velocity() const;
    void setVelocity(const QPoint& _velocity);
    bool puckOwner() const;
    void setPuckOwner(const bool owns);

    QPointF directionForce() const;
    void setDirectionForce(QPointF newDirectionForce);

    double rotationForce() const;
    void setRotationForce(double newRotationForce);

signals:
    void currentPosChanged();
    void positionChanged();
    void speedChanged();
    void targetReached();
    void targetPosChanged();
    void rotationChanged();
    void torqueChanged();
    void forceChanged();
    void userControlledChanged();
    void onTargetChanged();
    void velocityChanged();
    void puckOwnerChanged();

    void directionForceChanged();

    void rotationForceChanged();

public slots:

private:
    QString m_name;
    QString m_number;
    Type m_type;
    QPoint m_position;
    QPoint m_targetPos;
    QPoint m_currentPos;
    int m_speed;
    int m_weight;
    int m_stamina;
    int m_height;
    int m_maxSpeed;
    int m_id;
    int m_rotation;
    QPoint m_force;
    int m_torque;
    bool m_userControlled;
    bool m_onTarget;
    QPoint m_velocity;
    bool m_puckOwner;
    QPointF m_directionForce = QPointF(0,0);
    double m_rotationForce = 0.0;
};




#endif // PLAYER_H
