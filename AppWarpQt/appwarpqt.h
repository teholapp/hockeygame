#ifndef APPWARPQT_H
#define APPWARPQT_H

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QString>
#include "appwarpqt_global.h"



class AppWarpQtPrivate;

class APPWARPQTSHARED_EXPORT AppWarpQt : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(bool connected READ connected NOTIFY connectionStatusChanged)
    Q_PROPERTY(QObject* rooms READ rooms NOTIFY roomsChanged)
    Q_PROPERTY(QVariantMap connectedRoom READ connectedRoom NOTIFY connectedRoomChanged)

    explicit AppWarpQt(QObject *parent=0);
    virtual ~AppWarpQt();
    Q_INVOKABLE void connectToServer(const QString& playerName);
    Q_INVOKABLE void getAllRooms();
    Q_INVOKABLE void joinRoom(const QString& roomId);
    Q_INVOKABLE void sendChat(const QString& msg);
    bool connected() const;
    QObject* rooms();
    QVariantMap connectedRoom() const;
signals:
    void connectionStatusChanged();
    void roomsChanged();
    void connectedRoomChanged();
    void chatReceived(QString msg);

protected:
     AppWarpQtPrivate * d_ptr;
private:
     Q_DECLARE_PRIVATE(AppWarpQt);
};

#endif // APPWARPQT_H
