import QtQuick
import Felgo

EntityBase {
    id: cornerArea
    entityType: "wall"

    Component.onCompleted: {
    }

    Repeater {
        id: repeater
        model: 4
        property var prevX: 0
        property var prevY: 0
        property var startAngle: 180
        property var endAngle: 270
        property var step: Math.round(90 / 4)


        PolygonCollider {
           id: collider
           bodyType: Body.Static
           density: 0
           friction: 0.5
           restitution: 0.1

           Component.onCompleted: calcCorner()

           function calcXPos(i) {
                return Math.round(Math.cos((Math.PI/180.0)*(repeater.startAngle + (repeater.step * i)))*cornerArea.width + cornerArea.width)
           }

           function calcYPos(i) {
                return Math.round(Math.sin((Math.PI/180.0)*(repeater.startAngle + (repeater.step * i)))*cornerArea.width + cornerArea.width)
           }

           function calcPrevCorner()
            {
                   return Qt.point(calcXPos(index - 1), calcYPos(index - 1))
            }


           function calcCorner()
            {
               console.log("x: " + calcXPos(index) + " y: " + calcYPos(index))
               return Qt.point(calcXPos(index), calcYPos(index))
            }

           vertices: [
               calcPrevCorner(),
               calcCorner(),
               Qt.point(calcXPos(index -1), calcYPos(index))
           ]
         }
    }

}
