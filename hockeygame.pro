# allows to add DEPLOYMENTFOLDERS and links to the V-Play library and QtCreator auto-completion
CONFIG += Felgo
QT += qml core network statemachine widgets quick qml
qmlFolder.source = qml
DEPLOYMENTFOLDERS += qmlFolder # comment for publishing

assetsFolder.source = assets
DEPLOYMENTFOLDERS += assetsFolder

# Add more folders to ship with the application here

RESOURCES += \ #    resources.qrc # uncomment for publishing
    player_parts.qrc

# NOTE: for PUBLISHING, perform the following steps:
# 1. comment the DEPLOYMENTFOLDERS += qmlFolder line above, to avoid shipping your qml files with the application (instead they get compiled to the app binary)
# 2. uncomment the resources.qrc file inclusion and add any qml subfolders to the .qrc file; this compiles your qml files and js files to the app binary and protects your source code
# 3. change the setMainQmlFile() call in main.cpp to the one starting with "qrc:/" - this loads the qml files from the resources
# for more details see the "Deployment Guides" in the V-Play Documentation

# during development, use the qmlFolder deployment because you then get shorter compilation times (the qml files do not need to be compiled to the binary but are just copied)
# also, for quickest deployment on Desktop disable the "Shadow Build" option in Projects/Builds - you can then select "Run Without Deployment" from the Build menu in Qt Creator if you only changed QML files; this speeds up application start, because your app is not copied & re-compiled but just re-interpreted


# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    aiengine.cpp \
    player.cpp \
    line.cpp \
    team.cpp \
    aiplayer.cpp \
    aiutils.cpp \
    playergenerator.cpp \
    gamecontroller.cpp \
    contextengine.cpp \
    jsconsole.cpp \
    pathfinding.cpp \
    areatriggercontext.cpp

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += android/AndroidManifest.xml
}

ios {
    QMAKE_INFO_PLIST = ios/Project-Info.plist
    OTHER_FILES += $$QMAKE_INFO_PLIST
}

DISTFILES += \
    qml/LeftTopCornerWall.qml \
    qml/BottomLeftCornerWall.qml \
    qml/BottomRightCornerWall.qml \
    qml/LeftGoal.qml \
    qml/Defender.qml \
    qml/LeftDefender.qml \
    qml/RightDefender.qml \
    qml/Main.qml \
    qml/TeamSettings.qml \
    rules/center.qs \
    qml/PlayerItem.qml \
    qml/TeamContainer.qml

HEADERS += \
    aiengine.h \
    player.h \
    line.h \
    team.h \
    aiplayer.h \
    aiutils.h \
    aiplayerpositions.h \
    playergenerator.h \
    gamecontroller.h \
    contextengine.h \
    jsconsole.h \
    searchcell.h \
    pathfinding.h \
    areatriggercontext.h


# win32:CONFIG(release, debug|release): LIBS += -L$$PWD/AppWarpQt/release/ -lAppWarpQt
# else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/AppWarpQt/debug/ -lAppWarpQt
# else: android:include(AppWarpQt/AppWarpQt.pri)
# else:unix: LIBS += -L$$PWD/AppWarpQt/ -lAppWarpQt


# INCLUDEPATH += $$PWD/AppWarpQt
# DEPENDPATH += $$PWD/AppWarpQt/debug
