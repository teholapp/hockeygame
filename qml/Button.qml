import QtQuick
import QtGraphicalEffects

Item {
    id: button
    signal clicked
    property alias text: btnText.text
    property bool selectable: false
    property bool selected: false
    property bool pressed: false
    height: 50

    Rectangle {
        id: rect
        anchors.fill: parent
        radius: 8
        border.color:"#375264"

        gradient: pressed || selected ?onn:off

        Gradient {
            id:off
            GradientStop { position: 0.0; color: "#59cce9" }
            GradientStop { position: 1.0; color: "#3f6679" }
        }

        Gradient {
            id:onn
            GradientStop { position: 0.0; color: "#3f6679" }
            GradientStop { position: 1.0; color: "#59cce9" }
        }

        Text {
            id:btnText
            anchors.centerIn:parent
            color:"white"
            text: "text"
            font.pixelSize: 24
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            onClicked:  {
                button.clicked();
                if(selectable) selected = !selected
            }

            onPressed:{
                button.pressed = true
            }

            onReleased:{
                button.pressed = true
            }
        }
    }
    DropShadow {
        id: dropShadow
        anchors.fill: rect
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 16
        color: "#80000000"
        source: rect
        visible: !button.pressed || !button.selected
    }
}

