#include <QDebug>
#include "contextengine.h"

ContextSource::ContextSource(ContextEngine *aEngine, QObject *parent) :
    QObject(parent)
    ,m_engine(aEngine)
{
    m_engine->addSource(this);
}

ContextSource::~ContextSource()
{

}


QString ContextSource::name() {
    return m_name;
}

void ContextSource::setName(const QString& aName) {
    m_name = aName;
}


ContextRule::ContextRule(ContextEngine *aEngine, QObject *parent) :
    QObject(parent),
    m_engine(aEngine)
{
    QObject::connect(m_engine, &ContextEngine::contextChanged, this, &ContextRule::contextChanged);
}

ContextRule::~ContextRule()
{
}

void ContextRule::contextChanged(const QString& source, const QVariant& value)
{
    runRule(source, value);
}

void ContextRule::runRule(const QString& source, const QVariant& value)
{

}


ContextAction::ContextAction(QObject *parent) :
    QObject(parent),
    m_action("")
{

}

ContextAction::~ContextAction()
{

}


void ContextAction::setAction(const QString& aAction)
{
    m_action = aAction;
    emit actionChanged();
}

QString ContextAction::action() const
{
    return m_action;
}

ContextEngine::ContextEngine(QObject *parent)
    :QObject(parent)
{
}

ContextEngine::~ContextEngine()
{
}

void ContextEngine::addAction(ContextAction* action)
{
    m_actions.append(action);
}


void ContextEngine::addSource(const QString& sourceName, const QString& name, const QVariant& value)
{
    foreach (ContextSource *source, m_contextSources) {
        if(source->name() == sourceName) {
            source->addSource(name, value);
        }
    }
}

void ContextEngine::addSource(ContextSource *source)
{
    m_contextSources.append(source);
}

void ContextEngine::setContextVal(const QString& source, const QVariant& value)
{
    bool changed = false;
    QVariant val = m_contexts.value(source);
    if(!m_contexts.contains(source)) {
        m_contexts.insert(source, value);
        changed = true;
    }
    else if(val.isValid() && val != value) {
        m_contexts.insert(source, value);
        changed = true;
    }

    if(changed) {
        emit contextChanged(source, value);
    }
}


QVariant ContextEngine::contextValue(const QString& aName)
{
    if(m_contexts.contains(aName)) {
        return m_contexts.value(aName);
    }
    return QVariant();
}

void ContextEngine::runAction(const QString& target, const QString& action, const QVariant& aValue)
{
    qDebug() << "runAction: " << target << action << aValue;
    foreach(ContextAction *act, m_actions) {
        qDebug() << act->action();
        if(act->action() == target) {
            act->runAction(action, aValue);
        }
    }
}

void ContextEngine::changeContext(const QString& source, const QVariant& value)
{
    m_contexts[source] = value;
    emit contextChanged(source, value);
}

