#include <QPoint>
#include <QTime>
#include <QRect>
#include <QVector2D>
#include <QDebug>
#include <math.h>
#include <QFile>
#include <random>
#include <chrono>
#include "aiutils.h"

AIUtils::AIUtils(QObject *parent) : QObject(parent)
{
}

AIUtils::~AIUtils()
{

}

int AIUtils::getRandomNumber(int low, int high)
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());

    std::uniform_int_distribution<int> dist(low, high);
    return dist(rng);
}

void AIUtils::setGameAreaSize(QSize size)
{
    m_gameAreaSize = size;
    generateGrid();
}

AIUtils::HorizontalState AIUtils::puckHorizontalPosition(bool homeTeam, const QPoint& puck)
{
    if(puck.x() >= m_gameAreaSize.width() * 0.64) {
        if(!homeTeam)
            return AIUtils::Defence;
        else
            return AIUtils::Attack;

    }
    else if(puck.x() >= m_gameAreaSize.width() * 0.32) {
        return AIUtils::Neutral;
    }
    else {
        if(homeTeam)
            return AIUtils::Defence;
        else
            return AIUtils::Attack;
    }
}


AIUtils::VerticalState AIUtils::puckVerticalPosition(bool homeTeam, const QPoint& puck)
{
    if(puck.y() >= m_gameAreaSize.height() * 0.66) {
        if(homeTeam)
             return AIUtils::Right;
        else
            return AIUtils::Left;
    }
    else if(puck.y() >= m_gameAreaSize.height() * 0.33) {
        return AIUtils::Middle;
    }
    else {
        if(homeTeam)
            return AIUtils::Left;
        else
            return AIUtils::Right;
    }
}

QPoint AIUtils::randomPosInRect(QRect rect)
{
    int xx = getRandomNumber(rect.x(), rect.x() + rect.width());
    int yy = getRandomNumber(rect.y(), rect.y() + rect.height());
    return QPoint(xx, yy);
}

QPoint AIUtils::translatePosByTeam(bool homeTeam, QPoint pos)
{
    if(homeTeam)
        return pos;
    else {
        QPoint translated;
        translated.setX(m_gameAreaSize.width() - pos.x());
        translated.setY(m_gameAreaSize.height() - pos.y());
        return translated;
    }
}

QRect AIUtils::translateRectByTeam(bool homeTeam, const QRect& rect)
{
    if(homeTeam)
        return rect;
    else {
        QPoint bottomRight = translatePosByTeam(homeTeam, rect.topLeft());
        QPoint topLeft = translatePosByTeam(homeTeam, rect.bottomRight());
        QRect translated(topLeft, bottomRight);
        return translated;
    }
}

void AIUtils::generateGrid()
{
    int step_x = m_gameAreaSize.width() / X_LENGTH;
    int step_y = m_gameAreaSize.height() / Y_LENGTH;

    for(int i=0;i<Y_LENGTH;i++) {
        for(int j=0;j<X_LENGTH;j++) {
            QRect area;
            area.setX(step_x * j);
            area.setY(step_y * i);
            area.setWidth(step_x);
            area.setHeight(step_y);
            m_grid.push_back(area);
        }
    }

}

QPoint AIUtils::gridToAbsolutePos(int x, int y)
{
    int step_x = m_gameAreaSize.width() / X_LENGTH;
    int step_y = m_gameAreaSize.height() / Y_LENGTH;

    int x1 = x*step_x;
    int y1 = y*step_y;

    QRect area(x1,y1, step_x, step_y);
    return randomPosInRect(area);
}

QVector2D AIUtils::absolutePosToGridIndex(const QPoint& pos)
{
    int step_x = m_gameAreaSize.width() / X_LENGTH;
    int step_y = m_gameAreaSize.height() / Y_LENGTH;

    int x = pos.x() / step_x;
    int y = pos.y() / step_y;
    return QVector2D(x,y);
}

QList<QRect>* AIUtils::grid()
{
    return &m_grid;
}

double AIUtils::degToRad(double deg)
{
    return deg * M_PI / 180.0;
}

double AIUtils::radToDeg(double rad)
{
    return rad * 180.0/M_PI;
}

int AIUtils::calcTorqueDir(int targetAngle, int currentAngle)
{
    while (currentAngle > 180) currentAngle -= 360;

    int totalRotation = targetAngle - currentAngle;

    while (totalRotation < -180) totalRotation += 360;
    while (totalRotation > 180) totalRotation -= 360;

    if (abs(totalRotation) < 10)
        return 0;
    else
        return totalRotation < 0?-1:1;
}

QString AIUtils::readFile(const QString& source)
{
    if (source.isEmpty())
        return "";

    QFile file(source);
    if (!file.open(QFile::ReadOnly))
        return "";

    QTextStream in(&file);
    QString text;
    text = in.readAll();
    file.close();
    return text;
}
