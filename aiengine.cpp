#include <QPoint>
#include <QDebug>
#include <QColor>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QDir>
#include "contextengine.h"
#include "aiengine.h"
#include "aiutils.h"
#include "aiplayer.h"
#include "team.h"
#include "player.h"
#include "line.h"
#include "jsconsole.h"
#include "pathfinding.h"
#include "areatriggercontext.h"

AIEngine::AIEngine(ContextEngine *contextEngine, QObject *parent) :
    QObject(parent),
    m_contextEngine(contextEngine),
    m_width(720),
    m_height(320),
    m_utils(new AIUtils(this))
{
    m_utils->setGameAreaSize(QSize(m_width, m_height));

    //Parse ai rules
    m_ruleEngine = new QJSEngine(this);
    QJSValue jsContextEngine = m_ruleEngine->newQObject(m_contextEngine);
    m_ruleEngine->globalObject().setProperty("contextEngine", jsContextEngine);

    m_areaHandler = new AreaTriggerContext(m_utils, m_contextEngine, this);
    QObject::connect(this, &AIEngine::puckPosChanged, m_areaHandler, &AreaTriggerContext::puckPosChanged);
    parseFormations();

    JSConsole *console = new JSConsole(this);
    QJSValue consoleObj = m_ruleEngine->newQObject(console);
    m_ruleEngine->globalObject().setProperty("console", consoleObj);


    //Evaluate center script
    QDir dir("assets");
    dir.setNameFilters(QStringList()<<"rulescript_*");

    QFileInfoList files = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries);

    foreach (QFileInfo file, files){
        if (!file.isDir()){
            qDebug() << "FILE: " << file.absoluteFilePath();
            QString rule = m_utils->readFile(file.absoluteFilePath());
            QJSValue result = m_ruleEngine->evaluate(rule);

            if (result.isError())
                qDebug()
                    << "Uncaught exception at line"
                    << result.property("lineNumber").toInt()
                    << ":" << result.toString();

        }
    }
}

AIEngine::~AIEngine()
{

}

void AIEngine::parseFormations()
{
    QDir dir("assets");
    dir.setNameFilters(QStringList()<<"formation_*");

    QFileInfoList files = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries);

    foreach (QFileInfo file, files){
        if (!file.isDir()){
            qDebug() << "FILE: " << file.absoluteFilePath();
            QString formation = m_utils->readFile(file.absoluteFilePath());

            QJsonDocument formationJson = QJsonDocument::fromJson(formation.toUtf8());
            QJsonObject jsonObject = formationJson.object();
            QString formationName = jsonObject["name"].toString();
            QJsonArray jsonArray = jsonObject["formation"].toArray();

            QJSValue val = m_ruleEngine->evaluate(formation, file.fileName());

            m_ruleEngine->globalObject().setProperty(formationName, val);

/*            QMap<QString, QPoint> playersPos;
            foreach (const QJsonValue & value, jsonArray) {
                QJsonObject obj = value.toObject();
                int x = obj["x"].toInt();
                int y = obj["y"].toInt();
                QString player = obj["player"].toString();
                QPoint pos(x, y);
                playersPos.insert(player, pos);
            }
            m_formations.insert(formationName, playersPos);
            */
        }
    }

}

QPoint AIEngine::puckPos() const
{
    return m_puckPos;
}


void AIEngine::setPuckPos(const QPoint& newPos)
{
    m_puckPos = newPos;
    emit puckPosChanged(newPos);
}

void AIEngine::setHomeTeam(QObject* _team)
{
    m_homeTeam = dynamic_cast<Team*>(_team);
}

void AIEngine::setVisitorTeam(QObject* _team)
{
    m_visitorTeam = dynamic_cast<Team*>(_team);
}

QObject* AIEngine::homeTeam() const
{
    return dynamic_cast<QObject*>(m_homeTeam);
}

QObject* AIEngine::visitorTeam() const
{
    return dynamic_cast<QObject*>(m_visitorTeam);
}

int AIEngine::height() const
{
    return m_height;
}

void AIEngine::setHeight(int _height)
{
    m_height = _height;
}

int AIEngine::width() const
{
    return m_width;
}

void AIEngine::setWidth(int _width)
{
    m_width = _width;
}

void AIEngine::initialize()
{
    initTeam(m_homeTeam);

    initTeam(m_visitorTeam);
}

int AIEngine::pixelsPerMeter() const
{
    return m_pixelsPerMeter;
}

void AIEngine::setPixelsPerMeter(const int val)
{
    qDebug() << "pixelspermeter: " << val;
    m_pixelsPerMeter = val;
    emit pixelsPerMeterChanged();
}

void AIEngine::initTeam(Team *team)
{
    QList<QObject*>::iterator i;
    QList<QObject*> lines = team->m_lines;
    for (i = lines.begin(); i != lines.end(); ++i) {
        Line* line = dynamic_cast<Line*>(*i);
        QList<Player*> players = line->m_players;
        QList<Player*>::iterator j;
        for (j = players.begin(); j != players.end(); ++j) {
            Player* player = *j;
            AIPlayer *aiplayer = new AIPlayer(team->homeTeam(), player, this);
            m_aiPlayers.append(aiplayer);
            switch(player->type())
            {
            case Player::Center:
                player->setPosition(m_utils->translatePosByTeam(team->homeTeam(), QPoint(m_width / 2 - 20, m_height / 2)));
                break;

            case Player::LeftDefender:
                player->setPosition(m_utils->translatePosByTeam(team->homeTeam(), QPoint(m_width *0.4, m_height *0.4)));
                break;

            case Player::RightDefender:
                player->setPosition(m_utils->translatePosByTeam(team->homeTeam(), QPoint(m_width * 0.4, m_height * 0.6)));
                break;

            case Player::RightWing:
                player->setPosition(m_utils->translatePosByTeam(team->homeTeam(), QPoint(m_width / 2 - 20, m_height * 0.7)));
                break;

            case Player::LeftWing:
                player->setPosition(m_utils->translatePosByTeam(team->homeTeam(), QPoint(m_width / 2 - 20, m_height * 0.33)));
                break;
            }
        }
    }
}

void AIEngine::homeTeamGotPuck()
{
    qDebug() << "homeTeamGotPuck";
    m_contextEngine->setContextVal("puckOwner", "home");
    emit homeTeamGotPuckSignal();
}

void AIEngine::visitorTeamGotPuck()
{
    m_contextEngine->setContextVal("puckOwner", "visitor");
    emit visitorTeamGotPuckSignal();
}

void AIEngine::noOneHasPuck()
{
    emit noOneHasPuckSignal();
    m_contextEngine->setContextVal("puckOwner", "");
}


QList<QPoint> AIEngine::calcPathTo(QPoint from, QPoint to)
{
    QMap<int, bool> blockedPos;

    Line *home =  m_homeTeam->line();
    Line *visitor =  m_visitorTeam->line();

    QList<Player*>homeplayers = home->m_players;
    foreach (Player* plobject, homeplayers) {
        Player *player = plobject;
        QVector2D gridPos = m_utils->absolutePosToGridIndex(player->position());
        int id = m_utils->id(gridPos.x(), gridPos.y());
        blockedPos[id] = true;
    }
    QList<Player*>visitorPlayers = visitor->m_players;
    foreach (Player* plobject, visitorPlayers) {
        Player *player = plobject;
        QVector2D gridPos = m_utils->absolutePosToGridIndex(player->position());
        int id = m_utils->id(gridPos.x(), gridPos.y());
        blockedPos[id] = true;
    }

    QScopedPointer<PathFinding> path(new PathFinding());
    path->setBlockedCells(blockedPos);

    QVector2D start = m_utils->absolutePosToGridIndex(from);
    QVector2D end = m_utils->absolutePosToGridIndex(to);

    int i = 0;
    do {
        path->findPath(start, end);
    } while(!path->m_foundGoal && i++ < 15*10); //TODO 15*10 change to something wiser


    QList<QPoint> positions;

    for(int j=path->m_pathToGoal.length()-1;j>=0;j--) {
        //qDebug() << path->m_pathToGoal[j]->x() << path->m_pathToGoal[j]->y();
        QPoint pos = m_utils->gridToAbsolutePos(path->m_pathToGoal[j]->x(), path->m_pathToGoal[j]->y());
        positions.append(pos);
    }

    return positions;

}
