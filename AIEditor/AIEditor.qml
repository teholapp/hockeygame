import QtQuick 2.0

Item {
    id: root
//720
//320
    property real ratioVal: width / 720

    function scaleVal(val) {
        return Math.round(val/ratioVal)
    }

    Rectangle {
        anchors.fill: parent
        color: "grey"

        Item {
            id: gameItems
            anchors.fill: parent

            Image {
                width: ratioVal * 720
                height: ratioVal * 320
                source: "../assets/ring_image.png"
                visible: true
            }

            Grid {
                anchors.fill: parent
                columns: 30
                rows: 10
                spacing: 0
                Repeater {
                    model: 10*30
                    Rectangle {
                        color: "transparent"
                        border.color: "black"
                        border.width: 1
                        opacity: 0.8
                        width: gameItems.width / 30 * ratioVal
                        height: gameItems.height / 10 * ratioVal
                    }
                }
            }
        }

    }
}

