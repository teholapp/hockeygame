#include <QImage>
#include <QPainter>
#include <QSize>
#include <QString>
#include <QBitmap>
#include <QRegion>
#include <QDebug>
#include "playergenerator.h"

PlayerGenerator::PlayerGenerator()
    : QQuickImageProvider(Image),
      m_player(0),
      m_tmpImg(":/background")
{
    m_skateColor = Qt::black;
    m_glowesColor = Qt::blue;
    m_helmetColor = Qt::cyan;
    m_pantsColor = Qt::green;
    m_sockColor = Qt::yellow;
    m_upperBodyColor = Qt::red;
}

PlayerGenerator::~PlayerGenerator()
{

}


QImage PlayerGenerator::requestImage(const QString &id, QSize * size, const QSize & requestedSize)
{
    qDebug() << "id: " << id << " size: " << requestedSize;
    QStringList setup = id.split("/");

    if(m_player)
        delete(m_player);

    QString type = "";
    if(setup.length() == 7) {
        type = setup[0];
        m_skateColor = QColor(setup[1]);
        m_sockColor = QColor(setup[2]);
        m_pantsColor = QColor(setup[3]);
        m_glowesColor = QColor(setup[4]);
        m_upperBodyColor = QColor(setup[5]);
        m_helmetColor = QColor(setup[6]);
    }

    if(type == "")
        return QImage();

    if(type == "skater") {
        QSize rsize = requestedSize;
        if(requestedSize.width() == -1 || requestedSize.height() == -1) {
            rsize.setHeight(170);
            rsize.setWidth(82*3);
        }
        m_width = rsize.width() / 3;
        m_height = rsize.height();

        m_player = new QImage(
                (int) rsize.width(), (int) rsize.height(),
                    QImage::Format_ARGB32);
        m_player->fill(0);
        createSkater(0);
        createSkater(1);
        createSkater(2);
        *size = rsize;
    }
    else {
        m_player = new QImage(":/background_settings");
        m_player->fill(0);
        createSettings();
    }

    return *m_player;
}

void PlayerGenerator::createSkater(int spriteIndex)
{
    m_tmpImg.fill(0);
    QPainter painter(&m_tmpImg);
    int offsetX = (spriteIndex)*m_width;
    drawPart(&painter, generateFilename(":/skates", spriteIndex), m_skateColor);
    drawPart(&painter, generateFilename(":/socks", spriteIndex) , m_sockColor);
    drawPart(&painter, generateFilename(":/pants", spriteIndex) , m_pantsColor);
    drawPart(&painter, ":/stick", Qt::black);
    drawPart(&painter, ":/gloves", m_glowesColor);
    drawPart(&painter, ":/body", m_upperBodyColor);
    drawPart(&painter, ":/helmet", m_helmetColor);
    drawPart(&painter, generateFilename(":/foreground", spriteIndex) , Qt::black);
    //painter.drawImage(QRect(0,0, m_tmpImg.width(), m_tmpImg.height()), QImage(generateFilename(":/foreground", spriteIndex)));

    QImage scaled = m_tmpImg.scaled(m_width, m_height);
    QRect targetRect(offsetX,0, m_width, m_height);
    QPainter p(m_player);
    p.drawImage(targetRect, scaled);
}

void PlayerGenerator::createSlash(int spriteIndex)
{
    m_tmpImg.fill(0);
    QPainter painter(&m_tmpImg);
    int offsetX = spriteIndex*m_width;
    drawPart(&painter, generateFilename(":/skate", spriteIndex), m_skateColor);
    drawPart(&painter, generateFilename(":/sock", spriteIndex) , m_sockColor);
    drawPart(&painter, generateFilename(":/pants", spriteIndex) , m_pantsColor);
    drawPart(&painter, generateFilename(":/stick", spriteIndex), Qt::black);
    drawPart(&painter, generateFilename(":/glowes", spriteIndex), m_glowesColor);
    drawPart(&painter, generateFilename(":/upperbody", spriteIndex), m_upperBodyColor);
    drawPart(&painter, ":/helmet", m_helmetColor);

    QPainter p(m_player);
    QRect targetRect(offsetX,0, m_width, m_height);
    QImage rotated = m_tmpImg.transformed(QTransform().rotate(90.0));
    QImage scaled = rotated.scaled(m_width, m_height);
    p.drawImage(targetRect, scaled);
}

void PlayerGenerator::createSettings()
{
    QPainter painter(m_player);
    drawPart(&painter, ":/skate_settings", m_skateColor, 0);
    drawPart(&painter, ":/socks_settings" , m_sockColor, 0);
    drawPart(&painter, ":/pants_settings", m_pantsColor, 0);
    drawPart(&painter, ":/body_settings", m_upperBodyColor, 0);
    drawPart(&painter, ":/glowes_settings",  m_glowesColor, 0);
    drawPart(&painter, ":/stick_settings", Qt::black, 0);
    drawPart(&painter, ":/fase_settings", QColor(239, 209, 207), 0);
    drawPart(&painter, ":/helmet_settings", m_helmetColor, 0);
    drawPart(&painter, ":/shape_settings", Qt::black, 0);
}

 QString PlayerGenerator::generateFilename(const QString& filename, int spriteIndex)
 {
    return filename + QString("_") + QString::number(spriteIndex+1);

 }

void PlayerGenerator::drawPart(QPainter* painter, const QString& partSource, const QColor& color, bool gradient)
{
    QImage part(partSource);
    QImage alphaChannel = part.createAlphaMask();
    // Convert the alpha channel to a monochrome format
    QImage maskImage = alphaChannel.convertToFormat(QImage::Format_Mono);

    // Create a QBitmap from the monochrome image
    QBitmap maskBitmap = QBitmap::fromImage(maskImage);

    // Create a QRegion from the QBitmap
    QRegion maskRegion(maskBitmap);

    painter->setClipRegion(maskRegion);
    painter->setBrush(color);

    QRect rect(0,0, maskImage.width(), maskImage.height());
    if(gradient) {
        QLinearGradient gradient(rect.topLeft(), rect.bottomRight());
        gradient.setColorAt(0, color.lighter());
        gradient.setColorAt(1, color);
        painter->fillRect(rect, gradient);
    }
    else
        painter->fillRect(rect, QBrush(color));

}

