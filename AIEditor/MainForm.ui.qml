import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
    width: 300
    height: 200

    property alias button3: button3
    property alias button2: button2
    property alias button1: button1
}
