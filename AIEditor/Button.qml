import QtQuick

MouseArea {
    id: root
    property color activeColor: "lightgray"
    property color color: "#fdba00"
    property alias text: label.text
    property bool pressed: false
    property alias icon: icon.source
    property alias textColor: label.color
    property alias fontSize: label.font.pixelSize
    property alias iconSize: icon.height
    property alias textAlign: label.horizontalAlignment
    property int leftMargin: 10
    height: 30
    width: label.paintedWidth + icon.width + leftMargin * 2

    onPressed: root.pressed = true
    onReleased: root.pressed = false

    Rectangle {
        anchors.fill: parent
        radius: 3
        color: root.pressed?root.activeColor:root.color

        Image {
            id: icon
            height: parent.height * 0.8
            width: height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: root.leftMargin
        }

        Text {
            id: label
            font.pixelSize: 12
            color: "white"
            anchors.left: icon.source != ""?icon.right:parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignLeft
        }
    }
}
